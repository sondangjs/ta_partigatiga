package com.example.sonda.par33.Pedagang;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.API;
import com.example.sonda.par33.MapsActivity;
import com.example.sonda.par33.Model.KoordinatModel;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.example.sonda.par33.URLClass.URLClass;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPemesananPedagang extends AppCompatActivity {
    TextView namapelanggan, nama_barang, jumlah_beli, harga, totalBelanja, alamat, banyakBarang, ongkosKirim, totalPemba;
    ImageView gambarProduk;
    Button terima, tolak, lihatjalur;
    String data = "";
    UserModel userModel;
    List<Double> latitude = new ArrayList<>();
    List<Double> longitude = new ArrayList<>();
    LinearLayout linear ;
    int totalPembelian = 0;

    public static double jaraks = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemesanan_pedagang);

        userModel = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        linear = findViewById(R.id.tante);
        namapelanggan = findViewById(R.id.namapelanggan);
        jumlah_beli = findViewById(R.id.jumlahbeli);
        totalBelanja = findViewById(R.id.totalbelanja);
        alamat = findViewById(R.id.Alamat);
        ongkosKirim = findViewById(R.id.totalongkir);
        terima = findViewById(R.id.tombol3);
        tolak = findViewById(R.id.tombol2);
        lihatjalur = findViewById(R.id.tombol4);


        final Intent getData = getIntent();
        namapelanggan.setText(getData.getExtras().getString("namapelanggan"));
        jumlah_beli.setText(getData.getExtras().getString("jumlah_beli"));
        //MENAMPILKAN DETAIL BANYAK PEMESANAN PRODUK
        if(getData.getExtras().getString("nama_barang").contains(",")&& getData.getExtras().getString("totalBelanja").contains(",")
                && getData.getExtras().getString("harga").contains(",") && getData.getExtras().getString("gambar").contains(",") )
        {
            String namabarang = getData.getExtras().getString("nama_barang").replace(" ","");
            String totalBelanjas = getData.getExtras().getString("totalBelanja").replace(" ","");
            String hargap = getData.getExtras().getString("harga").replace(" ","");
            String gambar = getData.getExtras().getString("gambar").replace(" ","");
            System.out.println("Gambar : "+gambar);

            //MEMISAHKAN ATRIBUT PRODUK PERPRODUK.
            //MIS : BAYAM, KACANG
            String[] arrayofnamabarang = namabarang.split(",");
            String[] arrayoftotalbelanja = totalBelanjas.split(",");
            String[] arrayofHarga = hargap.split(",");
            String[] arrayofgambar = gambar.split(",");
            for( int i =0; i< arrayofnamabarang.length;i++){
                for (int a = i; a< arrayoftotalbelanja.length;a++){
                    for (int b = i; b< arrayofHarga.length;b++){
                        for (int c = i; c< arrayofgambar.length;c++){
                            //memastikan indeks array nya sama, kemudian fungsi dijalankan
                            if(i == a && i == b  && i == c){
                                // Layout inflanter adalah sebuah class yang bisa kita gunakan untuk membuat java object view 
                                //dari layout yang kita buat di xml. Layout Inflater digunakan jika kita ingin membuat sebuah sub activity, 
                                //jika kita gambarkan,fungsinya hampir sama seperti fragment, yaitu bagian kecil dari Activity.
                                View inflate = LayoutInflater.from(getApplicationContext()).inflate(R.layout.detail_item_pemesanan,null);
                                nama_barang = inflate.findViewById(R.id.namabarang);
                                banyakBarang = inflate.findViewById(R.id.totalBarang);
                                harga = inflate.findViewById(R.id.harga);
                                gambarProduk = inflate.findViewById(R.id.gambarProduk);
                                nama_barang.setText(arrayofnamabarang[i]);
                                banyakBarang.setText(arrayoftotalbelanja[a]);
                                harga.setText(arrayofHarga[b]);
                                Picasso.with(getApplicationContext()).load(arrayofgambar[c]).into(gambarProduk);
                                linear.addView(inflate);
                                //HARGA BARANG * JUMLAH BARANG YANG DI BELI
                                totalPembelian += Integer.parseInt(arrayoftotalbelanja[a]) * Integer.parseInt(arrayofHarga[b]);
                            }
                        }
                    }
                }
            }
            totalBelanja.setText(String.valueOf(totalPembelian));
        }
        //menampilkan 1 produk pemesanan
        else{
            View inflate = LayoutInflater.from(getApplicationContext()).inflate(R.layout.detail_item_pemesanan,null);
            nama_barang = inflate.findViewById(R.id.namabarang);
            banyakBarang = inflate.findViewById(R.id.totalBarang);
            harga = inflate.findViewById(R.id.harga);
            gambarProduk = inflate.findViewById(R.id.gambarProduk);
            nama_barang.setText(getData.getExtras().getString("nama_barang"));
            banyakBarang.setText(getData.getExtras().getString("totalBelanja"));
            harga.setText(getData.getExtras().getString("harga"));
            Picasso.with(getApplicationContext()).load(getData.getExtras().getString("gambar")).into(gambarProduk);
            linear.addView(inflate);

            //mencari total belanja (jumlah produk yang di beli * harga produk)
            totalPembelian = Integer.parseInt(getData.getExtras().getString("totalBelanja")) * Integer.parseInt(getData.getExtras().getString("harga"));
            totalBelanja.setText(String.valueOf(totalPembelian));
            //Integer digunakan untuk parsing atau 
            //mengkonversi nilai bertipe data string menjadi nilai bertipe data int
        }
        alamat.setText(getData.getExtras().getString("alamat"));


            terima.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getData.getExtras().getString("jumlah_beli").equals("Pemesanan dibatalkan")){

                                AlertDialog.Builder builder = new AlertDialog.Builder(DetailPemesananPedagang.this);

                                builder.setTitle("Konfirmasi");
                                builder.setMessage("Pesanan ini telah dibatalkan oleh pembeli.");

                                builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        // Do nothing
                                        dialog.dismiss();
                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                    }
                    else{
                        String status = "Pesanan Diantar";
                        UpdateStatus(getData.getExtras().getString("id_pembelian"), status);
                    }
                }
            });

            tolak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getData.getExtras().getString("jumlah_beli").equals("Pemesanan dibatalkan"))
                    {
                                AlertDialog.Builder builder = new AlertDialog.Builder(DetailPemesananPedagang.this);

                                builder.setTitle("Konfirmasi");
                                builder.setMessage("Pesanan ini telah dibatalkan oleh pembeli.");

                                builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        // Do nothing
                                        dialog.dismiss();
                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();

                    }
                    else{
                        final String status = "Pesanan Ditolak";

                        PopupMenu dropDown = new PopupMenu(getApplicationContext(), tolak);
                        dropDown.getMenuInflater().inflate(R.menu.menualasan, dropDown.getMenu());
                        dropDown.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()){
                                    case R.id.admin1:
                                        UpdateStatusTolak(getData.getExtras().getString("id_pembelian"), status,"produk habis");
                                        break;
                                }
                                return true;
                            }
                        });
                        dropDown.show();
                    }
                }
            });

//        else {
//            terima.setEnabled(false);
//            tolak.setEnabled(false);
//        }

        lihatjalur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String awal =  userModel.getAlamat();
                String tujuan = alamat.getText().toString();
                getAddress(awal, tujuan);
            }
        });


        //Set Harga Ongkir
        String asal = userModel.getAlamat();
        String tuju = alamat.getText().toString();
        API api = URLClass.getApiClient().create(API.class);
        Call<ResponseBody> responseBodyCall = api.getCoor(asal,tuju);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    data = new String(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String[] datas = data.split(",");
                API apiInterface = ClassURL.getApiClient().create(API.class);
                Call<List<KoordinatModel>> call1 = apiInterface.getKoordinate(Double.parseDouble(datas[0]), Double.parseDouble(datas[1]), Double.parseDouble(datas[2]), Double.parseDouble(datas[3]));
                call1.enqueue(new Callback<List<KoordinatModel>>() {
                    @Override
                    public void onResponse(Call<List<KoordinatModel>> call, Response<List<KoordinatModel>> response) {
//                for (KoordinatModel koor : response.body()){
//                    System.out.println("Lat : "+koor.getLat());
//                }
                        if(response.isSuccessful()){
                            for (KoordinatModel koor : response.body()){
                                latitude.add(koor.getLat());
                                longitude.add(koor.getLng());
                            }
                            ongkosKirim.setText(String.valueOf(jarakTotal(latitude,longitude)));
                            totalPemba = findViewById(R.id.totalPemba);
                            totalPemba.setText(String.valueOf(Integer.parseInt(totalBelanja.getText().toString()) + Integer.parseInt(ongkosKirim.getText().toString())));
                        }
                        else{
                            Log.d("Error 1 : ", response.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<KoordinatModel>> call, Throwable t) {
                        System.out.println("Error na : "+t.getMessage());
                    }
                });
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("'Error Na : "+t.getMessage());
            }
        });



    }

    private void UpdateStatusTolak(final String id_pembelians, final String status, final String alasan) {
        class UpdateState extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... strings) {

                String res = null;
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("id_pemesanan", id_pembelians));
                nameValuePair.add(new BasicNameValuePair("status_pembelian",status));
                nameValuePair.add(new BasicNameValuePair("alasan",alasan));
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost(ClassURL.URL+"Pedagang/Penolakan.php");
                    httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity httEntity = httpResponse.getEntity();
                    if(httEntity != null){
                        InputStream in = httEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        try {
                            while ((line = reader.readLine())!=null){
                                sb.append(line+"\n");
                            }
                            res = sb.toString();
                        }catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                in.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.d("response : ", res);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return res;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                try{
                    JSONObject obj = new JSONObject(s);

                    if(obj.getBoolean("status")){
                        String response = obj.getString("message");
                        if(response.equals("Sukses!")){
                            updatetotalharga(status);
                        }
                        else{
                            Toast.makeText(DetailPemesananPedagang.this, "Gagal!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(DetailPemesananPedagang.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("error na : "+e.getMessage());
                }
            }
        }
        UpdateState updateState = new UpdateState();
        updateState.execute(id_pembelians, status);
    }


    private void UpdateStatus(final String id_pembelian, final String status) {
       class UpdateState extends AsyncTask<String, Void, String>{

           @Override
           protected String doInBackground(String... strings) {

               String res = null;
               List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

               nameValuePair.add(new BasicNameValuePair("id_pembelian", id_pembelian));
               nameValuePair.add(new BasicNameValuePair("status_pembelian",status));

               try {
                   HttpClient httpClient = new DefaultHttpClient();
                   HttpPost httPost = new HttpPost(ClassURL.URL+"Pedagang/UpdateStatusPembelian.php");
                   httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                   HttpResponse httpResponse = httpClient.execute(httPost);
                   HttpEntity httEntity = httpResponse.getEntity();
                   if(httEntity != null){
                       InputStream in = httEntity.getContent();
                       BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                       StringBuilder sb = new StringBuilder();
                       String line = null;
                       try {
                           while ((line = reader.readLine())!=null){
                               sb.append(line+"\n");
                           }
                           res = sb.toString();
                       }catch (IOException e) {
                           e.printStackTrace();
                       } finally {
                           try {
                               in.close();
                           } catch (IOException e) {
                               e.printStackTrace();
                           }
                       }
                   }
                   Log.d("response : ", res);
               } catch (UnsupportedEncodingException e) {
                   e.printStackTrace();
               } catch (ClientProtocolException e) {
                   e.printStackTrace();
               } catch (IOException e) {
                   e.printStackTrace();
               }
               return res;
           }
           @Override
           protected void onPostExecute(String s) {
               super.onPostExecute(s);

               try{
                   JSONObject obj = new JSONObject(s);

                   if(obj.getBoolean("status")){
                       String response = obj.getString("message");
                       if(response.equals("Sukses!")){
                           Toast.makeText(DetailPemesananPedagang.this, "Pesanan Diterima\n Silahkan Kemas Produk Pemesanan", Toast.LENGTH_SHORT).show();
                           updatetotalharga(status);
                       }
                       else{
                           Toast.makeText(DetailPemesananPedagang.this, "Gagal!", Toast.LENGTH_SHORT).show();
                       }
                   }else {
                       Toast.makeText(DetailPemesananPedagang.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
                   System.out.println("error na : "+e.getMessage());
               }
           }
       }
       UpdateState updateState = new UpdateState();
       updateState.execute(id_pembelian, status);
    }

    private void updatetotalharga(final String hargaTot){
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {

                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable(){
                    public void run() {
                        jumlah_beli.setText(hargaTot);
                    }
                });

            }
        };
        new Thread(runnable).start();
    }
    void getAddress(String awal,String tujuan){
        API api = URLClass.getApiClient().create(API.class);
        Call<ResponseBody> responseBodyCall = api.getCoor(awal,tujuan);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    data = new String(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String[] datas = data.split(",");
                Intent in = new Intent(getApplicationContext(), MapsActivity.class);
                System.out.println("Lat0 : "+datas[0]);
                in.putExtra("latitude0", datas[0]);
                in.putExtra("longitude0", datas[1]);
                in.putExtra("latitude1", datas[2]);
                in.putExtra("longitude1",datas[3]);
                in.putExtra("jarak",String.valueOf(jaraks));
                startActivity(in);
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("'Error Na : "+t.getMessage());
            }
        });
    }



    private int jarakTotal(List<Double> latitude, List<Double> longitude) {
        //VARIABEL
        Double pi = 3.14159265358979323846;
        //JARIJARI BUMI DI UBAH KE METER X1000
        Double r = 637e3;
        Double latRad1, latRad2;
        Double DeltaLatRad;
        Double DeltaLongRad;
        Double a, c;
        Double hasil = 0.0;

        System.out.println("Latitude : "+latitude.size());

        for(int i = 0; i<latitude.size(); i++){
            for(int x = i;  x< longitude.size(); x++){
                if(i+1<latitude.size()){
                    latRad1 = latitude.get(i) * (pi/180);
                    latRad2 = latitude.get(i+1) * (pi/180);
                    DeltaLatRad = (latitude.get(i+1) - latitude.get(i)) * (pi/180);
                    DeltaLongRad = (longitude.get(i+1) - longitude.get(x)) * (pi/180);

                    //RUMUS HAVERSINE
                    a = Math.sin(DeltaLatRad/2)*Math.sin(DeltaLatRad/2) + Math.cos(latRad1) * Math.cos(latRad2) * Math.sin(DeltaLongRad/2) * Math.sin(DeltaLongRad/2);
                    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                    //HASIL JARAK DALAM METER
                    hasil += r * c;
                }else{
                    break;
                }
            }

        }
        //hasil di ubah ke Km
        jaraks = Math.ceil(hasil)/1000;
        System.out.println("Jarak : +/-"+Math.ceil(hasil)/1000+ " KM");

        //mencari harga ongkir per meter
        //ditetapkan 200Rp/meter.
        return (int) ((Math.ceil(hasil)/10)* 200);

    }

}
