package com.example.sonda.par33.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sonda on 4/2/2020.
 */

public class KategoriModel {
    @SerializedName("id_kategori")
    @Expose
    String id_kategori;
    @SerializedName("nama_kategori")
    @Expose
    String nama_kategori;
    @SerializedName("gambar")
    @Expose
    String gambar;

    public KategoriModel(String id_kategori, String nama_kategori, String gambar) {
        this.id_kategori = id_kategori;
        this.nama_kategori = nama_kategori;
        this.gambar = gambar;
    }

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
