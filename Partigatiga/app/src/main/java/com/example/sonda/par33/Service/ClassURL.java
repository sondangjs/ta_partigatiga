package com.example.sonda.par33.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by test on 12/28/2019.
 */

public class ClassURL {
    public static String URL="http://192.168.43.49/BackendPar33/";
    public static Retrofit retrofit;

    public static Retrofit getApiClient(){

        if(retrofit == null){
            Gson gson = new GsonBuilder().setLenient().create();
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retrofit;
    }
}
