package com.example.sonda.par33.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 2/28/2020.
 */

public class Jalur {
    @SerializedName("jalur")
    private String jalur;

    public String getJalur() {
        return jalur;
    }
}
