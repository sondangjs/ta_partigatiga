package com.example.sonda.par33.Pembeli;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;

import com.example.sonda.par33.API;
import com.example.sonda.par33.Adapter.HomeProdukAdapter;
import com.example.sonda.par33.Adapter.KategoriAdapter;
import com.example.sonda.par33.Adapter.ProdukAdapter;
import com.example.sonda.par33.Model.KategoriModel;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeProduk extends AppCompatActivity {
    GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_produk);

        API api = ClassURL.getApiClient().create(API.class);
        final Call<List<ProdukModel>> produkCall = api.getHomeProduk();
        produkCall.enqueue(new Callback<List<ProdukModel>>() {
            @Override
            public void onResponse(Call<List<ProdukModel>> call, Response<List<ProdukModel>> response) {
                if(response.isSuccessful()){
                    tampilkanProduk(response.body());
                    Log.d("response : ",response.body().toString());
                }else{
                    Log.e("Error Message",response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ProdukModel>> call, Throwable t) {
                Log.e(" Message",t.getMessage());
            }
        });
    }

    private void tampilkanProduk(List<ProdukModel> produk){
//        HomeProdukAdapter produkAdapter = new HomeProdukAdapter(getApplicationContext(), R.layout.home_produk_item, produk);
//        gridView = findViewById(R.id.gridView1);
//        gridView.setAdapter(produkAdapter);
//        produkAdapter.notifyDataSetChanged();
    }
}
