package com.example.sonda.par33.Model;

/**
 * Created by test on 3/23/2020.
 */

public class UserModel {
    private String id;
    private String nama;
    private String jenis_kelamin;
    private String alamat;
    private String email;
    private String n0_telp;
    private String role;
    private String username;
    private String image;


    public UserModel(String id, String nama, String jenis_kelamin, String alamat, String email, String n0_telp, String role, String username, String image) {
        this.id = id;
        this.nama = nama;
        this.jenis_kelamin = jenis_kelamin;
        this.alamat = alamat;
        this.email = email;
        this.n0_telp = n0_telp;
        this.role = role;
        this.username = username;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getN0_telp() {
        return n0_telp;
    }

    public void setN0_telp(String n0_telp) {
        this.n0_telp = n0_telp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
