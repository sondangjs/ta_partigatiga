package com.example.sonda.par33.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 3/13/2020.
 */

public class ModelAddress {
    @SerializedName("id_destination")
    String id_destination;
    @SerializedName("destination_Name")
    String destination_name;
    @SerializedName("destination_coordinate")
    String destination_coordiante;

    public String getId_destination() {
        return id_destination;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public String getDestination_coordiante() {
        return destination_coordiante;
    }
}
