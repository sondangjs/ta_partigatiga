package com.example.sonda.par33.Home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.example.sonda.par33.API;
import com.example.sonda.par33.Adapter.HomeProdukAdapter;
import com.example.sonda.par33.DataTroli;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {
    RecyclerView gridView;
    HomeProdukAdapter adapter;
    List<ProdukModel> produkModels;
    ViewFlipper v_flipper;
    ImageView myTroli;
    private RecyclerView.LayoutManager layoutManager;
    @SuppressLint("WrongViewCast")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view =  inflater.inflate(R.layout.fragment_home, container, false);

        //slider
        int images[] = {R.drawable.bunga, R.drawable.tomato, R.drawable.mujaer};

        gridView = view.findViewById(R.id.gridView1);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        gridView.setLayoutManager(layoutManager);
        gridView.setHasFixedSize(true);


        v_flipper = view.findViewById(R.id.test);
       //1

        for(int image: images){
            flipperImages(image);
        }

        myTroli = view.findViewById(R.id.myTroli);
        myTroli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DataTroli.class));
            }
        });

        fetchProduk();

       return view;
    }

    private void fetchProduk() {
        API api = ClassURL.getApiClient().create(API.class);
        final Call<List<ProdukModel>> produkCall = api.getHomeProduk();
        produkCall.enqueue(new Callback<List<ProdukModel>>() {
            @Override
            public void onResponse(Call<List<ProdukModel>> call, Response<List<ProdukModel>> response) {
                if(response.isSuccessful()){
                    produkModels = response.body();
                    adapter = new HomeProdukAdapter(produkModels, getContext());
                    gridView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    Log.d("response : ",response.body().toString());
                }else{
                    Log.e("Error Message",response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ProdukModel>> call, Throwable t) {
                Log.e(" Message",t.getMessage());
            }
        });
    }

    public void flipperImages(int image){
        ImageView imageView = new ImageView(getContext());
        imageView.setBackgroundResource(image);
        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(4200);
        v_flipper.setAutoStart(true);
        v_flipper.setInAnimation(getContext(),android.R.anim.slide_in_left);
        v_flipper.setOutAnimation(getContext(),android.R.anim.slide_out_right);
    }

}
