package com.example.sonda.par33;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.Model.Jalur;
import com.example.sonda.par33.Model.KoordinatModel;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.URLClass.JSONRetrofit;
import com.example.sonda.par33.URLClass.URLClass;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final int LOCATION_REQUEST = 500;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    MarkerOptions options, options1;
    List<LatLng> markerList = new ArrayList<>();
    Polyline poly = null;
    API api;
    List<Jalur> jalur;
    String latitude0, latitude1;
    String longitude0, longitude1;
    String dataService = "";
    String data = "";
    int regulation = 0;
    int indikator = 0;
    TextView teks;

    Geocoder geocoder;

    List<Address> addressList = null;
    List<Double> latitude = new ArrayList<>();
    List<Double> longitude = new ArrayList<>();
    double latAwal, latTujuan;
    double lngAwal, lngTujuan;
    String[] koordinat;
    LatLng myLocation, myLocation1;
    API apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Intent maxIntent = getIntent();
        latitude0  = maxIntent.getExtras().getString("latitude0");
        longitude0 = maxIntent.getExtras().getString("longitude0");
        latitude1 = maxIntent.getExtras().getString("latitude1");
        longitude1 = maxIntent.getExtras().getString("longitude1");
        System.out.println("1 : "+latitude0);
        System.out.println("2 : "+longitude0);
        System.out.println("3 : "+latitude1);
        System.out.println("4 : "+longitude1);
        teks= findViewById(R.id.search);
        teks.setText("Jarak : +/- "+maxIntent.getExtras().getString("jarak")+" Km");




//        fetchJalur(latitude0, longitude0, latitude1, longitude1);
//        data=new String(fetchJalur(latitude0, latitude1, longitude0, longitude1));

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LocationManager location = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            return;
        }
        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.getUiSettings().setZoomControlsEnabled(true);

        Criteria criteria = new Criteria();
        Location location1 = location.getLastKnownLocation(location.getBestProvider(criteria, false));
            Toast.makeText(this, "Lokasi Ditemukan", Toast.LENGTH_SHORT).show();

            apiInterface = ClassURL.getApiClient().create(API.class);
            Call<List<KoordinatModel>> call = apiInterface.getKoordinate(Double.parseDouble(latitude0), Double.parseDouble(longitude0), Double.parseDouble(latitude1), Double.parseDouble(longitude1));
            call.enqueue(new Callback<List<KoordinatModel>>() {
                @Override
                public void onResponse(Call<List<KoordinatModel>> call, Response<List<KoordinatModel>> response) {
//                for (KoordinatModel koor : response.body()){
//                    System.out.println("Lat : "+koor.getLat());
//                }
                    if(response.isSuccessful()){
                        for (KoordinatModel koor : response.body()){
                            System.out.println("AllKoor : "+koor.getLat());
                            latTujuan = koor.getLat();
                            lngTujuan = koor.getLng();
                            latitude.add(koor.getLat());
                            longitude.add(koor.getLng());
                            options = new MarkerOptions().position(new LatLng(koor.getLat(), koor.getLng())).title("test");
                            markerList.add(options.getPosition());
                        }
                        PolylineOptions polylineOptions = new PolylineOptions().addAll(markerList).clickable(true).color(Color.BLUE);
                        mMap.addPolyline(polylineOptions);
//                        System.out.println("Koordinate Awal : "+latAwal+lngAwal);

                        for (int i = 0; i<latitude.size(); i++){
                            latAwal = latitude.get(0);
                        }
                        for (int i = 0; i<longitude.size(); i++){
                            lngAwal = longitude.get(0);
                        }

                        myLocation = new LatLng(latTujuan, lngTujuan);
//                        myLocation1 = new LatLng(latAwal, lngAwal);
//                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 17));
                        mMap.addMarker(options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).title("Tujuan"));

                        options1 = new MarkerOptions().position(new LatLng(latAwal, lngAwal)).title("Awal");
                        myLocation1 = new LatLng(latAwal, lngAwal);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation1, 17));
                        mMap.addMarker(options1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).title("Tujuan"));
                    }
                    else{
                        Log.d("Error 1 : ", response.message());
                    }
                }

                @Override
                public void onFailure(Call<List<KoordinatModel>> call, Throwable t) {
                    System.out.println("Error na : "+t.getMessage());
                }
            });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    mMap.setMyLocationEnabled(true);
                }
                break;
        }
    }

}
