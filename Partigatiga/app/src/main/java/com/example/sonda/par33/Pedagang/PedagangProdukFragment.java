package com.example.sonda.par33.Pedagang;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.sonda.par33.API;
import com.example.sonda.par33.Adapter.DeleteAdapter;
import com.example.sonda.par33.Adapter.ProdukAdapter;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.DELETE;


public class PedagangProdukFragment extends Fragment {
    GridView gridView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View view= inflater.inflate(R.layout.fragment_pedagang_produk, container, false);

        UserModel userModel = SharedPrefManager.getInstance(getContext()).getUser();

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TambahProduk.class));
            }
        });

        API api = ClassURL.getApiClient().create(API.class);
        final Call<List<ProdukModel>> produkCall = api.getProdukPedagang(userModel.getId());
        produkCall.enqueue(new Callback<List<ProdukModel>>() {
            @Override
            public void onResponse(Call<List<ProdukModel>> call, Response<List<ProdukModel>> response) {
                if(response.isSuccessful()){
                    tampilkanProduk(response.body());
                }else{
                    Log.e("Error Message",response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ProdukModel>> call, Throwable t) {
                Log.e(" Message",t.getMessage());
            }
        });
        return view;
    }
    private void tampilkanProduk(List<ProdukModel> produk){
    DeleteAdapter produkAdapter = new DeleteAdapter(getContext(), R.layout.deleteproduk, produk);
    gridView = getActivity().findViewById(R.id.gridView1);
    gridView.setAdapter(produkAdapter);
    produkAdapter.notifyDataSetChanged();
}



}
