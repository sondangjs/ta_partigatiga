package com.example.sonda.par33.HomeSeller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.sonda.par33.R;
import com.example.sonda.par33.SharedPref.SharedPrefManager;


public class HomeSellerActivity extends AppCompatActivity {
    Button logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_seller);

        final SharedPrefManager sharedPrefManager = new SharedPrefManager(getApplicationContext());

        logout = (Button) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefManager.logout();
            }
        });

    }
}
