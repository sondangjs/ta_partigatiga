package com.example.sonda.par33.Pedagang;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.example.sonda.par33.Splashscreen;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class TambahProduk extends AppCompatActivity {
EditText nama_produk,harga,stok_produk, satuan;
AutoCompleteTextView kategori_produk;
CircleImageView gambar;
Button add;
String nama_produks, kategori_produks,hargas,stok_produks;

    private static final int STORAGE_PERMISSION_CODE = 4655;
    private int PICK_IMAGE_RESULT = 1;
    private Uri filepath;
    private Bitmap bitmap;
    ArrayList<String> kategori = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_produk);

        nama_produk= findViewById(R.id.nama_produk);
        kategori_produk = findViewById(R.id.kategori_produk);
        harga = findViewById(R.id.harga);
        stok_produk = findViewById(R.id.stok_produk);
        gambar = findViewById(R.id.profile_image);
        satuan = findViewById(R.id.satuan_produk);

        gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        add= findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage();
            }
        });

        ArrayAdapter<String>  adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kategori);
        kategori.add("Sayur");
        kategori.add("Daging");
        kategori.add("Ikan");
        kategori.add("Rempah");
        kategori.add("Buah");
        kategori.add("Bunga");
        kategori_produk.setAdapter(adapter);
        System.out.println("Kategori Produk : "+kategori_produk.getText().toString());
//        kategori_produk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                kategori_produk.setText();
//            }
//        });

        storagePermission();
    }


    private void storagePermission(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == STORAGE_PERMISSION_CODE){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_RESULT && data != null && data.getData() != null){
            filepath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                gambar.setImageBitmap(bitmap);
            }
            catch (Exception ex){
                Log.d("Error on : ", ex.getMessage());
            }
        }
    }

    private String getPath(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();

        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Images.Media._ID+"=?", new String[]{document_id},null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }
    private void uploadImage(){

        final UserModel userModel = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        String id_user = userModel.getId();
        String email = userModel.getEmail();

        nama_produks = nama_produk.getText().toString();
        if(kategori_produk.getText().toString().equals("Sayur")){
            kategori_produks="1";
        }else if (kategori_produk.getText().toString().equals("Daging")) {
            kategori_produks ="2";
        }else if (kategori_produk.getText().toString().equals("Ikan")) {
            kategori_produks ="3";
        }else if (kategori_produk.getText().toString().equals("Rempah")) {
            kategori_produks = "4";
        }else if (kategori_produk.getText().toString().equals("Buah")) {
            kategori_produks = "5";
        }else if(kategori_produk.getText().toString().equals("Bunga")) {
            kategori_produks = "6";
        }

        hargas = harga.getText().toString();
        stok_produks = stok_produk.getText().toString();
        System.out.println("id_kategori : "+kategori_produks);
        System.out.println("Data ; "+nama_produks);
        System.out.println("data ; "+hargas);
        System.out.println("data : "+stok_produks);
        System.out.println("Data ; "+id_user);
        String path =  getPath(filepath);
        System.out.println("imageLink : "+path);
        try {
            String uploadID = UUID.randomUUID().toString();

            new MultipartUploadRequest(this, uploadID, ClassURL.URL+"Pedagang/addProduk.php")
                    .addFileToUpload(path, "gambar")
                    .addParameter("id_kategori", kategori_produks)
                    .addParameter("nama_produk", nama_produks)
                    .addParameter("harga", hargas)
                    .addParameter("stok_produk", stok_produks)
                    .addParameter("id_pedagang", id_user)
                    .addParameter("satuan", satuan.getText().toString())

//                    .setNotificationConfig(new UploadNotificationConfig())
//                    .setMaxRetries(3)
                    .startUpload();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        startActivity(new Intent(getApplicationContext(), Splashscreen.class));
        this.finish();
    }


}
