package com.example.sonda.par33;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;


public class EditProfile extends AppCompatActivity {
    EditText nama, alamat, phone;
    String names, address, phones, id_user, email;
    Button editProfil;
    CircleImageView profile;

    private static final int STORAGE_PERMISSION_CODE = 4655;
    private int PICK_IMAGE_RESULT = 1;
    private Uri filepath;
    private Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);


        nama = (EditText) findViewById(R.id.name);
        alamat = (EditText) findViewById(R.id.alamat);
        phone = (EditText) findViewById(R.id.phone);
        profile = (CircleImageView) findViewById(R.id.profile_image);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        Intent get = getIntent();
        nama.setText(get.getExtras().getString("nama"));
        alamat.setText(get.getExtras().getString("alamat"));
        phone.setText(get.getExtras().getString("phone"));
        Picasso.with(getApplicationContext()).load(get.getExtras().getString("image")).into(profile);


        editProfil = (Button) findViewById(R.id.edit);
        editProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });
    }

//    private void storagePermission(){
//
//    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_RESULT && data != null && data.getData() != null){
            filepath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                profile.setImageBitmap(bitmap);
            }
            catch (Exception ex){
                Log.d("Error on : ", ex.getMessage());
            }
        }
    }

    private String getPath(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();

        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Images.Media._ID+"=?", new String[]{document_id},null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    private void uploadImage(){

        final UserModel userModel = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        id_user = userModel.getId();
        email = userModel.getEmail();
        names = nama.getText().toString();
        address = alamat.getText().toString();
        phones = phone.getText().toString();
        String path =  getPath(filepath);
        String image = "http://192.168.43.49/BackendPar33/Profile/IMG_"+id_user+"_"+names+".jpg";
        System.out.println("Path : "+path);
        UserModel userModel1 = new UserModel(id_user, names, userModel.getJenis_kelamin(), address, email, phones, userModel.getRole(), userModel.getUsername(), image);
        SharedPrefManager.getInstance(getApplicationContext()).UserLogin(userModel1);

        try {
            String uploadID = UUID.randomUUID().toString();
            System.out.println("Name : "+names);
            System.out.println("Image Path : "+path);
            new MultipartUploadRequest(this, uploadID, ClassURL.URL+"Autentikasi/EditProfile.php")
                    .addFileToUpload(path, "gambar")
                    .addParameter("nama", names)
                    .addParameter("alamat", address)
                    .addParameter("phone", phones)
                    .addParameter("id_user", id_user)
                    .addParameter("email", email)
//                    .setNotificationConfig(new UploadNotificationConfig())
//                    .setMaxRetries(3)
                    .startUpload();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        startActivity(new Intent(getApplicationContext(), Splashscreen.class));
        this.finish();
    }
}
