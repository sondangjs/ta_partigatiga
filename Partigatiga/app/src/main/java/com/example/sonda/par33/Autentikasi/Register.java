package com.example.sonda.par33.Autentikasi;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicNameValuePair;
import com.example.sonda.par33.API;
import com.example.sonda.par33.CheckRoleActivity;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Pembeli.Checkout;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.example.sonda.par33.URLClass.JSONRetrofit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Register extends AppCompatActivity {
    EditText nama, jenis_kelamin, no_telp, email, username, password;
    AutoCompleteTextView alamat;
    String namas, jenisKelamins, no_telps, emails, usernames, passwords, alamats;
    Button daftar;
    String role;
    String destinations="";
    API api;
    ArrayList<String> array = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Intent get = getIntent();
        role = get.getExtras().getString("role");

        Toast.makeText(this, "Role : "+role , Toast.LENGTH_SHORT).show();

        nama = (EditText) findViewById(R.id.namalengkap);
        jenis_kelamin = (EditText) findViewById(R.id.jeniskelamin);
        no_telp = (EditText) findViewById(R.id.notelp);
        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        alamat = findViewById(R.id.alamat);

        api = JSONRetrofit.getApiClient().create(API.class);
        Call<ResponseBody> result = api.getAllAdress();
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    destinations = new String(response.body().string());
                    String[] datas = destinations.split(";");
                    for(int j=0; j<datas.length; j++){
                        array.add(datas[j]);
//                        System.out.println("Data : "+datas[j]);
                    }

                } catch (IOException e) {
                    System.out.println("Error na : "+e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Register.this, "Terdapat Gangguan! Silahkan Coba Lagi", Toast.LENGTH_SHORT).show();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        alamat.setAdapter(adapter);

        daftar = (Button) findViewById(R.id.daftar);
        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nama.getText().toString().equals("") || jenis_kelamin.getText().toString().equals("") ||
                        no_telp.getText().toString().equals("") || email.getText().toString().equals("") ||
                        username.getText().toString().equals("") || password.getText().toString().equals("") ||
                        alamat.getText().toString().equals("")){
                    Toast.makeText(Register.this, "Form Harus Diisi", Toast.LENGTH_SHORT).show();
                }
                else{
                    namas = nama.getText().toString();
                    jenisKelamins = jenis_kelamin.getText().toString();
                    no_telps = no_telp.getText().toString();
                    emails = email.getText().toString();
                    usernames = username.getText().toString();
                    passwords = password.getText().toString();
                    alamats = alamat.getText().toString();

                    RegisterUser(namas, jenisKelamins, no_telps, emails, usernames, passwords, alamats);
                }
            }
        });
    }

    private void RegisterUser(final String namas, final String jenisKelamins, final String no_telps, final String emails, final String usernames, final String passwords, final String alamats) {
        class registerUser extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                String res = null;
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("nama", namas));
                nameValuePair.add(new BasicNameValuePair("jenis_kelamin", jenisKelamins));
                nameValuePair.add(new BasicNameValuePair("alamat", alamats));
                nameValuePair.add(new BasicNameValuePair("email", emails));
                nameValuePair.add(new BasicNameValuePair("no_telp", no_telps));
                nameValuePair.add(new BasicNameValuePair("role", role));
                nameValuePair.add(new BasicNameValuePair("username", usernames));
                nameValuePair.add(new BasicNameValuePair("password", passwords));

                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost(ClassURL.URL+"Autentikasi/register.php");
                    httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity httEntity = httpResponse.getEntity();
                    if(httEntity != null){
                        InputStream in = httEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        try {
                            while ((line = reader.readLine())!=null){
                                sb.append(line+"\n");
                            }
                            res = sb.toString();
                        }catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                in.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.d("response : ", res);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return res;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                try{
                    JSONObject obj = new JSONObject(s);

                    if(obj.getBoolean("status")){
                        String response = obj.getString("message");
                        if(response.equals("Username already exist!")){
                            Toast.makeText(Register.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                            nama.setText("");
                            password.setText("");
                            alamat.setText("");
                            no_telp.setText("");
                            email.setText("");
                            username.setText("");
                            jenis_kelamin.setText("");
                        }
                        else{
                            JSONObject userJSON = obj.getJSONObject("DataUser");

                            UserModel user = new UserModel(
                                    userJSON.getString("id_user"),
                                    userJSON.getString("nama"),
                                    userJSON.getString("jenis_kelamin"),
                                    userJSON.getString("alamat"),
                                    userJSON.getString("email"),
                                    userJSON.getString("no_telp"),
                                    userJSON.getString("role"),
                                    userJSON.getString("username"),
                                    userJSON.getString("image")
                            );

                            SharedPrefManager.getInstance(getApplicationContext()).UserLogin(user);
                            finish();
//                            Toast.makeText(Register.this, userJSON.getString("role"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), CheckRoleActivity.class));
                        }
                    }else {
                        Toast.makeText(Register.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("error na : "+e.getMessage());
                }
            }
        }
        registerUser register = new registerUser();
        register.execute(namas, jenisKelamins, no_telps, emails, usernames, passwords, alamats);
    }

}
