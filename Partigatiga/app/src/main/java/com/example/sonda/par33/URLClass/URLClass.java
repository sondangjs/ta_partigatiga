package com.example.sonda.par33.URLClass;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by test on 12/28/2019.
 */

public class URLClass {
    public static String URL="http://192.168.43.49/BackEndPar33/AlgoritmaDjikstra/";
    public static Retrofit retrofit;

    public static Retrofit getApiClient(){

        if(retrofit == null){
            Gson gson = new GsonBuilder().setLenient().create();
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(ScalarsConverterFactory.create()).build();
        }
        return retrofit;
    }

}
