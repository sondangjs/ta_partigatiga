package com.example.sonda.par33.SharedPref;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.sonda.par33.Autentikasi.Login;
import com.example.sonda.par33.CheckRoleActivity;
import com.example.sonda.par33.MainActivity;
import com.example.sonda.par33.Model.UserModel;


/**
 * Created by test on 1/1/2020.
 */

public class SharedPrefManager {
    private static final String SHARED_NAME = "shared";
    private static final String ID = "id_user";
    private static final String NAME = "nama";
    private static final String JENIS_KELAMIN = "jenis_kelamin";
    private static final String ALAMAT = "alamat";
    private static final String EMAIL = "email";
    private static final String NO_TELP = "no_telp";
    private static final String USERNAME = "username";
    private static final String IMAGE = "image";
    private static final String ROLE = "role";


    private static SharedPrefManager mInstance;
    private static Context mCtx;

    public SharedPrefManager(Context context){
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void UserLogin(UserModel userModel){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ID, userModel.getId());
        editor.putString(NAME, userModel.getNama());
        editor.putString(JENIS_KELAMIN, userModel.getJenis_kelamin());
        editor.putString(ALAMAT, userModel.getAlamat());
        editor.putString(EMAIL, userModel.getEmail());
        editor.putString(NO_TELP, userModel.getN0_telp());
        editor.putString(ROLE, userModel.getRole());
        editor.putString(USERNAME, userModel.getUsername());
        editor.putString(IMAGE, userModel.getImage());


        editor.apply();
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USERNAME, null)!=null;
    }

    public UserModel getUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        return new UserModel(
                sharedPreferences.getString(ID,null),
                sharedPreferences.getString(NAME, null),
                sharedPreferences.getString(JENIS_KELAMIN, null),
                sharedPreferences.getString(ALAMAT,null),
                sharedPreferences.getString(EMAIL, null),
                sharedPreferences.getString(NO_TELP, null),
                sharedPreferences.getString(ROLE, null),
                sharedPreferences.getString(USERNAME, null),
                sharedPreferences.getString(IMAGE, null)
        );
    }

    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        mCtx.startActivity(new Intent(mCtx, Login.class));
    }

    public void CheckLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(mCtx, Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mCtx.startActivity(i);
        }else{
                Intent i = new Intent(mCtx, CheckRoleActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mCtx.startActivity(i);
        }
    }
}
