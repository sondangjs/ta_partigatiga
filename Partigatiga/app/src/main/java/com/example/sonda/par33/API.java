package com.example.sonda.par33;

import com.example.sonda.par33.Model.AlasanModel;
import com.example.sonda.par33.Model.KategoriModel;
import com.example.sonda.par33.Model.KeranjangModel;
import com.example.sonda.par33.Model.KoordinatModel;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.Model.pemesananPembeli;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by test on 2/28/2020.
 */

public interface API {
    @GET("CariRute.php")
    Call<ResponseBody> getJalur(@Query("latitude0") String latitude0,
                                @Query("latitude1") String latitude1,
                                @Query("longitude0") String longitude0,
                                @Query("longitude1") String longitude1);

    @GET("getAllCoor.php")
    Call<ResponseBody> getAllCoor(@Query("latitude") String lat,
                                  @Query("longitude") String longitude);

    @GET("getAllDestination.php")
    Call<ResponseBody> getAllAdress();

    @GET("GetCoorAddress.php")
    Call<ResponseBody> getCoor(@Query("awal") String awal,
                               @Query("tujuan") String tujuan);

    @GET("Pembeli/getKategori.php")
    Call<List<KategoriModel>>getKategori();

    @GET("Pembeli/getProduk.php")
    Call<List<ProdukModel>>getProduk(@Query("key") String key);


    @POST("Pedagang/getProdukPedagang.php")
    Call<List<ProdukModel>>getProdukPedagang(@Query("id_pedagang") String id_pedagang);

    @GET("Pembeli/getPemesananPembeli.php")
    Call<List<pemesananPembeli>>getPemesananmPembeli(@Query("id_user") String id_pembeli);

    @GET("Pedagang/getPemesananPedagang.php")
    Call<List<pemesananPembeli>>getPemesananmPedagang(@Query("id_pedagang") String id_pedagang);

    @GET("getPemesananKeranjang.php")
    Call<List<KeranjangModel>> getKeranjang(@Query("id_pembeli")String id_pembeli);

    @GET("CobaAlgoritma/Main.php")
    Call<List<KoordinatModel>> getKoordinate(@Query("lat0") double lat0,
                                             @Query("lng0") double lng0,
                                             @Query("lat1") double lat1,
                                             @Query("lng1") double lng1);

    @GET("Pembeli/HomeProduk.php")
    Call<List<ProdukModel>>getHomeProduk();

    @GET("Pembeli/getAlasan.php")
    Call<List<AlasanModel>> getAlasan(@Query("id_pemesanan") String id_pemesanan);
}
