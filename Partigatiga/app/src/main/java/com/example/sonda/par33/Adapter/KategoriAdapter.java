package com.example.sonda.par33.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.Model.KategoriModel;
import com.example.sonda.par33.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sonda on 4/2/2020.
 */

public class KategoriAdapter extends ArrayAdapter<KategoriModel>{
    Context context;
    public KategoriAdapter(@NonNull Context context, int resource, @NonNull List<KategoriModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.kategori_item, parent, false
            );
        }

        Picasso.Builder picassos = new Picasso.Builder(context);
        final KategoriModel kategori = getItem(position);

        TextView nama = (TextView) convertView.findViewById(R.id.text);
        nama.setText(kategori.getNama_kategori());
        ImageView image = (ImageView) convertView.findViewById(R.id.image);

        Picasso picasso = picassos.build();
        picasso.load(kategori.getGambar()).into(image);


        convertView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Toast.makeText(context, "Kategori : "+kategori.getNama_kategori(), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        return convertView;
    }
}
