package com.example.sonda.par33.Pembeli;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.API;
import com.example.sonda.par33.Autentikasi.Register;
import com.example.sonda.par33.CheckRoleActivity;
import com.example.sonda.par33.DataTroli;
import com.example.sonda.par33.GetAllDestination;
import com.example.sonda.par33.Home.HomeActivity;
import com.example.sonda.par33.MainActivity;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Pedagang.MenuPedagang;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.example.sonda.par33.URLClass.JSONRetrofit;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Checkout extends AppCompatActivity {
    EditText  jumlah_beli;
    ImageView gambarProduk;
    TextView total_belanja, namapelanggan, nama_brang,  harga;
    AutoCompleteTextView alamat;
    Button hargaTot, batal, pesan;
    String destinations = "";
    String id_pedagang, id_produk;
    API api;
    ArrayList<String> array = new ArrayList<>();
    UserModel user;
    String gambar_produk;
    String stok_produk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        Picasso.Builder picassos = new Picasso.Builder(getApplicationContext());

        UserModel userModel = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        namapelanggan = findViewById(R.id.namapelanggan);
        nama_brang = findViewById(R.id.namabarang);
        jumlah_beli = findViewById(R.id.jumlahbeli);
        harga = findViewById(R.id.harga);
        total_belanja = findViewById(R.id.totalbelanja);
        hargaTot = findViewById(R.id.hartot);
        alamat = findViewById(R.id.Alamat);
       // total_belanja.getText().toString();
        batal = findViewById(R.id.tombol2);
        pesan = findViewById(R.id.tombol3);
        gambarProduk = findViewById(R.id.gambarProduk);

        final Intent in = getIntent();
        nama_brang.setText(in.getExtras().getString("nama_produk"));
        harga.setText(in.getExtras().getString("harga_produk"));
        user = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        System.out.println("User Name : "+user.getNama());
        namapelanggan.setText(user.getNama());
        Picasso picasso = picassos.build();
        picasso.load(in.getExtras().getString("gambar")).into(gambarProduk);
        id_pedagang = in.getExtras().getString("id_pedagang");
      //  Toast.makeText(this, "id_pedagan"+id_pedagang, Toast.LENGTH_SHORT).show();
        id_produk = in.getExtras().getString("id_produk");
        stok_produk = in.getExtras().getString("stokProduk");

        gambar_produk = in.getExtras().getString("gambar");


        alamat.setText(userModel.getAlamat());

        hargaTot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hargas = Integer.parseInt(harga.getText().toString());
                int jlh = Integer.parseInt(jumlah_beli.getText().toString());
                int hasil = jlh * hargas;
                updatetotalharga(String.valueOf(hasil));
            }
        });



        api = JSONRetrofit.getApiClient().create(API.class);
        Call<ResponseBody> result = api.getAllAdress();
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    destinations = new String(response.body().string());
                    String[] datas = destinations.split(";");
                    for(int j=0; j<datas.length; j++){
                        array.add(datas[j]);
//                        System.out.println("Data : "+datas[j]);
                    }

                } catch (IOException e) {
                    System.out.println("Error na : "+e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(Checkout.this, "Terdapat Gangguan! Silahkan Coba Lagi", Toast.LENGTH_SHORT).show();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        alamat.setAdapter(adapter);

        pesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(jumlah_beli.getText().toString().equals("") || alamat.getText().toString().equals("")){
                    Toast.makeText(Checkout.this, "Form Harus Diisi", Toast.LENGTH_SHORT).show();
                }
                else{
                    if (Integer.parseInt(jumlah_beli.getText().toString()) > Integer.parseInt(in.getStringExtra("stokProduk"))) {
                        Toast.makeText(Checkout.this, "Pesanan anda melebihi stok barang", Toast.LENGTH_SHORT).show();
                    }else{
                        addToTroli(user.getId(), jumlah_beli.getText().toString(), id_produk);
                    }

                }

            }
        });

        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jumlah_beli.getText().toString().equals("") || alamat.getText().toString().equals("")){
                    Toast.makeText(Checkout.this, "Form Harus Diisi", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (Integer.parseInt(jumlah_beli.getText().toString()) > Integer.parseInt(in.getStringExtra("stokProduk"))) {
                        Toast.makeText(Checkout.this, "Pesanan anda melebihi stok barang", Toast.LENGTH_SHORT).show();
                    } else {

                        pesanProduk();
                    }
                }
            }
        });
    }
    private void updatetotalharga(final String hargaTot){
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {

                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.post(new Runnable(){
                        public void run() {
                            total_belanja.setText(hargaTot);
                        }
                    });

            }
        };
        new Thread(runnable).start();
    }

    public String getCurrentDate(){
        DateFormat date = new java.text.SimpleDateFormat("dd-M-yy");
        java.util.Date dates = new java.util.Date();
        String waktu = date.format(dates);
        return waktu;
    }

    private void pesanProduk(){
        class Pesan extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... strings) {
                String id_user = user.getId();
                String alamats = alamat.getText().toString();
                String tanggal = getCurrentDate();
                String total = jumlah_beli.getText().toString();
                String status = "DiPesan";

                String res = null;
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("id_user", id_user));
                nameValuePair.add(new BasicNameValuePair("tanggal_beli", tanggal));
                nameValuePair.add(new BasicNameValuePair("alamat_beli", alamats));
                nameValuePair.add(new BasicNameValuePair("id_produk", id_produk));
                nameValuePair.add(new BasicNameValuePair("id_pedagang", id_pedagang));
                nameValuePair.add(new BasicNameValuePair("status_pembelian",status));
                nameValuePair.add(new BasicNameValuePair("jumlah_dibeli", total));
                nameValuePair.add(new BasicNameValuePair("nama_produk", nama_brang.getText().toString()));
                nameValuePair.add(new BasicNameValuePair("harga_produk", harga.getText().toString()));
                nameValuePair.add(new BasicNameValuePair("nama_pembeli", user.getNama()));
                nameValuePair.add(new BasicNameValuePair("kontak_pembeli", user.getN0_telp()));
                nameValuePair.add(new BasicNameValuePair("gambar_produk", gambar_produk));
                nameValuePair.add(new BasicNameValuePair("stok", String.valueOf(Integer.parseInt(stok_produk) - Integer.parseInt(total))));

                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost(ClassURL.URL+"Pembeli/pemesanan.php");
                    httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity httEntity = httpResponse.getEntity();
                    if(httEntity != null){
                        InputStream in = httEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        try {
                            while ((line = reader.readLine())!=null){
                                sb.append(line+"\n");
                            }
                            res = sb.toString();
                        }catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                in.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.d("response : ", res);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e){
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return res;
            }
            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);


                try{
                    JSONObject obj = new JSONObject(s);

                    if(obj.getBoolean("status")){
                        String response = obj.getString("message");
                        if(response.equals("Pembelian Sukses!")){
                            Toast.makeText(Checkout.this, "Selamat! Pembelian Anda Sedang Diproses.", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                            finish();
                        }
                        else{
                            Toast.makeText(Checkout.this, "Maaf Pembelian Gagal!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(Checkout.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("error na : "+e.getMessage());
                }
            }
        }
        Pesan pesan = new Pesan();
        pesan.execute() ;
    }

    //function to add Data to cart
    private void addToTroli(final String id_pembeli,final String total_dibeli, final String id_produk) {
        class AddTroli extends AsyncTask<String, Void, String>{
            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("id_pembeli", id_pembeli));
                nameValuePair.add(new BasicNameValuePair("id_produk", id_produk));
                nameValuePair.add(new BasicNameValuePair("jumlah_dibeli", total_dibeli));
                nameValuePair.add(new BasicNameValuePair("alamat", alamat.getText().toString()));
                nameValuePair.add(new BasicNameValuePair("stok", String.valueOf(Integer.parseInt(stok_produk) - Integer.parseInt(total_dibeli))));
                try{
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost("http://192.168.43.49/BackendPar33/Pemesanan.php");
                    httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity httEntity = httpResponse.getEntity();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return "Data Insert Successfully";
            }
        }
        AddTroli add = new AddTroli();
        add.execute(id_pembeli,total_dibeli, id_produk);

        startActivity(new Intent(getApplicationContext(), DataTroli.class));
        finish();
    }

}
