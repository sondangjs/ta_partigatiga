package com.example.sonda.par33.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sonda on 4/3/2020.
 */

public class ProdukModel {
    @SerializedName("id_produk")
    @Expose
    String id_produk;
    @SerializedName("id_kategori")
    @Expose
    String id_kategori;
    @SerializedName("nama_produk")
    @Expose
    String nama_produk;
    @SerializedName("harga")
    @Expose
    String harga;
    @SerializedName("stok_produk")
    @Expose
    String stok_produk;
    @SerializedName("gambar")
    @Expose
    String gambar;
    @SerializedName("id_pedagang")
    @Expose
    String id_pelanggan;

    @SerializedName("nama")
    @Expose
    String nama;
    @SerializedName("alamat")
    @Expose
    String alamat;
    @SerializedName("no_telepon")
    @Expose
    String no_telepon;
    @SerializedName("satuan")
    @Expose
    String satuan;

    public ProdukModel(String id_produk, String id_kategori, String nama_produk, String harga, String stok_produk, String gambar, String id_pelanggan, String nama, String alamat, String no_telepon, String satuan) {
        this.id_produk = id_produk;
        this.id_kategori = id_kategori;
        this.nama_produk = nama_produk;
        this.harga = harga;
        this.stok_produk = stok_produk;
        this.gambar = gambar;
        this.id_pelanggan = id_pelanggan;
        this.nama = nama;
        this.alamat = alamat;
        this.no_telepon = no_telepon;
        this.satuan = satuan;
    }

    public String getId_produk() {
        return id_produk;
    }

    public void setId_produk(String id_produk) {
        this.id_produk = id_produk;
    }

    public String getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(String id_kategori) {
        this.id_kategori = id_kategori;
    }

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStok_produk() {
        return stok_produk;
    }

    public void setStok_produk(String stok_produk) {
        this.stok_produk = stok_produk;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getId_pelanggan() {
        return id_pelanggan;
    }

    public void setId_pelanggan(String id_pelanggan) {
        this.id_pelanggan = id_pelanggan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_telepon() {
        return no_telepon;
    }

    public void setNo_telepon(String no_telepon) {
        this.no_telepon = no_telepon;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }
}