package com.example.sonda.par33.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.Home.DetailProdukActivity;
import com.example.sonda.par33.Model.KategoriModel;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sonda on 4/3/2020.
 */

public class ProdukAdapter extends ArrayAdapter<ProdukModel> {
    Context context;
    public ProdukAdapter(@NonNull Context context, int resource, @NonNull List<ProdukModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.produk_item, parent, false
            );
        }

        Picasso.Builder picassos = new Picasso.Builder(context);
        final ProdukModel produk = getItem(position);

        TextView desc = (TextView) convertView.findViewById(R.id.desc);
        TextView harga = (TextView) convertView.findViewById(R.id.harga);
        desc.setText(produk.getNama_produk());
        harga.setText(produk.getHarga()+"/"+produk.getSatuan());
        ImageView image = (ImageView) convertView.findViewById(R.id.icon);
        TextView stok = convertView.findViewById(R.id.stokproduk);
        stok.setText(produk.getStok_produk());
        Picasso picasso = picassos.build();
        picasso.load(produk.getGambar()).into(image);




        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailProdukActivity.class);
                intent.putExtra("id_produk", produk.getId_produk());
                intent.putExtra("id_pedagang", produk.getId_pelanggan());
                intent.putExtra("gambar", produk.getGambar());
                intent.putExtra("nama_produk", produk.getNama_produk());
                intent.putExtra("harga_produk", produk.getHarga());
                intent.putExtra("stok_produk", produk.getStok_produk());
                intent.putExtra("id_pedagang", produk.getId_pelanggan());
                intent.putExtra("satuan", produk.getSatuan());
                intent.putExtra("LayoutCome","1");
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }
}

