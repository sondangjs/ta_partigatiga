package com.example.sonda.par33.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.API;
import com.example.sonda.par33.MapsActivity;
import com.example.sonda.par33.Model.AlasanModel;
import com.example.sonda.par33.Model.KoordinatModel;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Model.pemesananPembeli;
import com.example.sonda.par33.Pedagang.DetailPemesananPedagang;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.example.sonda.par33.URLClass.URLClass;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sonda on 4/5/2020.
 */

public class PemesananPembeliAdapter extends RecyclerView.Adapter<PemesananPembeliAdapter.MyViewHolder>{

    ImageView image;
    String data = "";
    private List<pemesananPembeli> pemesananPembelis;
    private Context context;
    List<Double> latitude = new ArrayList<>();
    List<Double> longitude = new ArrayList<>();
    public static double jaraks = 0;

    public PemesananPembeliAdapter(List<pemesananPembeli> pemesananPembelis, Context context) {
        this.pemesananPembelis = pemesananPembelis;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pemesanan_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PemesananPembeliAdapter.MyViewHolder holder, final int position) {
        if(pemesananPembelis.get(position).getNama_produk().contains(",")){
            StringBuilder builder1 = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            StringBuilder builder3 = new StringBuilder();
            String[] arrayOfNama = pemesananPembelis.get(position).getNama_produk().split(",");
            String[] arrayOfharga = pemesananPembelis.get(position).getHarga().split(",");
            String[] arrayOftotal = pemesananPembelis.get(position).getTotal_beli().split(",");
            String[] arrayOfImage = pemesananPembelis.get(position).getGambar().split(",");

            for(String a : arrayOfNama){
                builder1.append(a);
                builder1.append("\n\n");
            }
            holder.nama_barang.setText(builder1.toString());

            for(String b : arrayOfharga){
                builder2.append(b);
                builder2.append("\n\n");
            }
            holder.harga_barang.setText(builder2.toString());

            for(String c : arrayOftotal){
                builder3.append(c);
                builder3.append("\n\n");
            }
            holder.total_barang.setText(builder3.toString());

            for(int i=0; i<arrayOfImage.length;i++) {
                if(arrayOfImage[i].contains(" ")){
                    View inflate = LayoutInflater.from(context).inflate(R.layout.image_item, null);
                    image = inflate.findViewById(R.id.iv);
                    Picasso.with(context).load(arrayOfImage[i].replace(" ","")).into(image);
                    holder.linearLayout.addView(inflate);
                }
                else {
                    View inflate = LayoutInflater.from(context).inflate(R.layout.image_item, null);
                    image = inflate.findViewById(R.id.iv);
                    Picasso.with(context).load(arrayOfImage[i]).into(image);
                    holder.linearLayout.addView(inflate);
                }
            }

            String hargas = pemesananPembelis.get(position).getHarga().replace(" ","");
            String jumlah = pemesananPembelis.get(position).getTotal_beli().replace(" ", "");
            String[] arrayOfHargas = hargas.split(",");
            String[] arrayJumlah = jumlah.split(",");
            //
            int hartot = 0;
            int cariHarga= 0;
            for (int i = 0; i<arrayOfHargas.length; i++){
               for (int j = 0; j<arrayJumlah.length; j++){
                   if(i==j){
                       cariHarga = (Integer.parseInt(arrayOfHargas[i]) * Integer.parseInt(arrayJumlah[j]));
                       hartot += cariHarga;
                   }
               }
            }
            holder.harga_total.setText(String.valueOf(hartot));
            holder.ongkir.setText("Total Ongkir  : "+String.valueOf(jarakTotal(latitude,longitude)));


            if(pemesananPembelis.get(position).getStatus_pembelian().equals("Pesanan Ditolak")){
                holder.textTolak.setVisibility(View.VISIBLE);

                API api = ClassURL.getApiClient().create(API.class);
                Call<List<AlasanModel>> call = api.getAlasan(pemesananPembelis.get(position).getId_beli());
                call.enqueue(new Callback<List<AlasanModel>>() {
                    @Override
                    public void onResponse(Call<List<AlasanModel>> call, Response<List<AlasanModel>> response) {
                        for (AlasanModel alasanModel : response.body()){
                            holder.textTolak.setText(alasanModel.getAlasan());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<AlasanModel>> call, Throwable t) {

                    }
                });
            }

            else{
                holder.textTolak.setVisibility(View.GONE);
            }

            holder.status.setText(pemesananPembelis.get(position).getStatus_pembelian());


            holder.cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(pemesananPembelis.get(position).getStatus_pembelian().equals("Pesanan Diantar")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Mohon Maaf. Pesanan Anda sedang dalam perjalanan\nAnda tidak bisa membatalkan pemesanan ini.");

                        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                    else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Apakah Anda Yakin Ingin Membatalkan Pemesanan ini?");

                        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing but close the dialo
                                String status = "Pemesanan dibatalkan";
                                UpdateStatusTolak(pemesananPembelis.get(position).getId_beli(),status);
                                dialog.dismiss();
                                dialog.dismiss();
                            }
                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // Do nothing
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            });


            String asal = pemesananPembelis.get(position).getAlamat_pedagang();
            String tuju = pemesananPembelis.get(position).getAlamat_beli();
            API api = URLClass.getApiClient().create(API.class);
            Call<ResponseBody> responseBodyCall = api.getCoor(asal,tuju);
            responseBodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        data = new String(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String[] datas = data.split(",");
                    API apiInterface = ClassURL.getApiClient().create(API.class);
                    Call<List<KoordinatModel>> call1 = apiInterface.getKoordinate(Double.parseDouble(datas[0]), Double.parseDouble(datas[1]), Double.parseDouble(datas[2]), Double.parseDouble(datas[3]));
                    call1.enqueue(new Callback<List<KoordinatModel>>() {
                        @Override
                        public void onResponse(Call<List<KoordinatModel>> call, Response<List<KoordinatModel>> response) {
//                for (KoordinatModel koor : response.body()){
//                    System.out.println("Lat : "+koor.getLat());
//                }
                            if(response.isSuccessful()){
                                for (KoordinatModel koor : response.body()){
                                    latitude.add(koor.getLat());
                                    longitude.add(koor.getLng());
                                }
//                                ongkosKirim.setText(String.valueOf(jarakTotal(latitude,longitude)));
                                System.out.println("Jarak : "+jarakTotal(latitude,longitude));
//                                totalPemba = findViewById(R.id.totalPemba);
//                                totalPemba.setText(String.valueOf(Integer.parseInt(totalBelanja.getText().toString()) + Integer.parseInt(ongkosKirim.getText().toString())));
                            }
                            else{
                                Log.d("Error 1 : ", response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<List<KoordinatModel>> call, Throwable t) {
                            System.out.println("Error na : "+t.getMessage());
                        }
                    });
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    System.out.println("'Error Na : "+t.getMessage());
                }
            });

        }
        else{
            final int hartots;
            holder.nama_barang.setText(pemesananPembelis.get(position).getNama_produk());
            holder.harga_barang.setText(pemesananPembelis.get(position).getHarga());
            holder.total_barang.setText(pemesananPembelis.get(position).getTotal_beli());
            View inflate = LayoutInflater.from(context).inflate(R.layout.image_item, null);
            image = inflate.findViewById(R.id.iv);
            Picasso.with(context).load(pemesananPembelis.get(position).getGambar()).into(image);
            holder.linearLayout.addView(inflate);
//            System.out.println("Total Harga :"+pemesananPembelis.get(position).getTotal_beli());

            hartots = Integer.parseInt(pemesananPembelis.get(position).getHarga()) * Integer.parseInt(pemesananPembelis.get(position).getTotal_beli());

            holder.status.setText(pemesananPembelis.get(position).getStatus_pembelian());

            String asal = pemesananPembelis.get(position).getAlamat_pedagang();
            String tuju = pemesananPembelis.get(position).getAlamat_beli();
            API api = URLClass.getApiClient().create(API.class);
            Call<ResponseBody> responseBodyCall = api.getCoor(asal,tuju);
            responseBodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        data = new String(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String[] datas = data.split(",");
                    API apiInterface = ClassURL.getApiClient().create(API.class);
                    Call<List<KoordinatModel>> call1 = apiInterface.getKoordinate(Double.parseDouble(datas[0]), Double.parseDouble(datas[1]), Double.parseDouble(datas[2]), Double.parseDouble(datas[3]));
                    call1.enqueue(new Callback<List<KoordinatModel>>() {
                        @Override
                        public void onResponse(Call<List<KoordinatModel>> call, Response<List<KoordinatModel>> response) {
//                for (KoordinatModel koor : response.body()){
//                    System.out.println("Lat : "+koor.getLat());
//                }
                            if(response.isSuccessful()){
                                for (KoordinatModel koor : response.body()){
                                    latitude.add(koor.getLat());
                                    longitude.add(koor.getLng());
                                }
//                                ongkosKirim.setText(String.valueOf(jarakTotal(latitude,longitude)));
                                System.out.println("Jarak : "+jarakTotal(latitude,longitude));
//                                totalPemba = findViewById(R.id.totalPemba);
//                                totalPemba.setText(String.valueOf(Integer.parseInt(totalBelanja.getText().toString()) + Integer.parseInt(ongkosKirim.getText().toString())));
                                  holder.harga_total.setText(String.valueOf(hartots + jarakTotal(latitude,longitude)));
                                  holder.ongkir.setText("Total Ongkir : "+String.valueOf(jarakTotal(latitude,longitude)));
                            }
                            else{
                                Log.d("Error 1 : ", response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<List<KoordinatModel>> call, Throwable t) {
                            System.out.println("Error na : "+t.getMessage());
                        }
                    });
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    System.out.println("'Error Na : "+t.getMessage());
                }
            });

            if(pemesananPembelis.get(position).getStatus_pembelian().equals("Pesanan Ditolak")){
                holder.textTolak.setVisibility(View.VISIBLE);
                api = ClassURL.getApiClient().create(API.class);
                Call<List<AlasanModel>> call = api.getAlasan(pemesananPembelis.get(position).getId_beli());
                call.enqueue(new Callback<List<AlasanModel>>() {
                    @Override
                    public void onResponse(Call<List<AlasanModel>> call, Response<List<AlasanModel>> response) {
                        for (AlasanModel alasanModel : response.body()){
                            holder.textTolak.setText(alasanModel.getAlasan());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<AlasanModel>> call, Throwable t) {

                    }
                });
            }
            else{
                holder.textTolak.setVisibility(View.GONE);
            }
                holder.cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(pemesananPembelis.get(position).getStatus_pembelian().equals("Pesanan Diantar")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);

                            builder.setTitle("Konfirmasi");
                            builder.setMessage("Mohon Maaf. Pesanan Anda sedang dalam perjalanan\nAnda tidak bisa membatalkan pemesanan ini.");

                            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Do nothing
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                        else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);

                            builder.setTitle("Konfirmasi");
                            builder.setMessage("Apakah Anda Yakin Ingin Membatalkan Pemesanan ini?");

                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing but close the dialo
                                    String status = "Pemesanan dibatalkan";
                                    UpdateStatusTolak(pemesananPembelis.get(position).getId_beli(),status);
                                    dialog.dismiss();
                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Do nothing
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                });

        }

    }

    @Override
    public int getItemCount() {
        return pemesananPembelis.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView nama_barang;
        TextView harga_barang;
        TextView total_barang;
        TextView harga_total;
        LinearLayout linearLayout;
        TextView textTolak;
        TextView status;
        TextView ongkir;
        Button cancelBtn;
        public MyViewHolder(View itemView) {
            super(itemView);
            nama_barang = itemView.findViewById(R.id.textView8);
            harga_barang = itemView.findViewById(R.id.textView9);
            total_barang = itemView.findViewById(R.id.textViews);
            harga_total = itemView.findViewById(R.id.editText);
            linearLayout = itemView.findViewById(R.id.imageView2);
            textTolak = itemView.findViewById(R.id.textTolak);
            status = itemView.findViewById(R.id.textView5);
            cancelBtn = itemView.findViewById(R.id.cancelBtn);
            ongkir = itemView.findViewById(R.id.ongkir);
        }
    }
    private void UpdateStatusTolak(final String id_pembelians, final String status) {
        class UpdateState extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... strings) {

                String res = null;
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("id_pemesanan", id_pembelians));
                nameValuePair.add(new BasicNameValuePair("status_pembelian",status));

                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost(ClassURL.URL+"Pembeli/PembatanPemesanan.php");
                    httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity httEntity = httpResponse.getEntity();
                    if(httEntity != null){
                        InputStream in = httEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        try {
                            while ((line = reader.readLine())!=null){
                                sb.append(line+"\n");
                            }
                            res = sb.toString();
                        }catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                in.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.d("response : ", res);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return res;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                try{
                    JSONObject obj = new JSONObject(s);

                    if(obj.getBoolean("status")){
                        String response = obj.getString("message");
                        if(response.equals("Sukses!")){
                            Toast.makeText(context,"Pemesanan anda telah dibatalkan",Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(context, "Gagal!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(context, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("error na : "+e.getMessage());
                }
            }
        }
        UpdateState updateState = new UpdateState();
        updateState.execute(id_pembelians, status);
    }

    private int jarakTotal(List<Double> latitude, List<Double> longitude) {
        //VARIABEL
        Double pi = 3.14159265358979323846;
        //JARIJARI BUMI DI UBAH KE METER X1000
        Double r = 637e3;
        Double latRad1, latRad2;
        Double DeltaLatRad;
        Double DeltaLongRad;
        Double a, c;
        Double hasil = 0.0;

        System.out.println("Latitude : "+latitude.size());

        for(int i = 0; i<latitude.size(); i++){
            for(int x = i;  x< longitude.size(); x++){
                if(i+1<latitude.size()){
                    latRad1 = latitude.get(i) * (pi/180);
                    latRad2 = latitude.get(i+1) * (pi/180);
                    DeltaLatRad = (latitude.get(i+1) - latitude.get(i)) * (pi/180);
                    DeltaLongRad = (longitude.get(i+1) - longitude.get(x)) * (pi/180);

                    //RUMUS HAVERSINE
                    a = Math.sin(DeltaLatRad/2)*Math.sin(DeltaLatRad/2) + Math.cos(latRad1) * Math.cos(latRad2) * Math.sin(DeltaLongRad/2) * Math.sin(DeltaLongRad/2);
                    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                    //HASIL JARAK DALAM METER
                    hasil += r * c;
                }else{
                    break;
                }
            }

        }
        //hasil di ubah ke Km
        jaraks = Math.ceil(hasil)/1000;
        System.out.println("Jarak : +/-"+Math.ceil(hasil)/1000+ " KM");

        //mencari harga ongkir per meter
        //ditetapkan 200Rp/meter.
        return (int) ((Math.ceil(hasil)/10)* 20);

    }

}
