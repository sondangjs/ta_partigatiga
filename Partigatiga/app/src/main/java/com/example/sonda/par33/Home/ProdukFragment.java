package com.example.sonda.par33.Home;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sonda.par33.API;
import com.example.sonda.par33.Adapter.KategoriAdapter;
import com.example.sonda.par33.Adapter.ProdukAdapter;
import com.example.sonda.par33.Model.KategoriModel;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProdukFragment extends Fragment {
    GridView gridView;
    Spinner spinner;
    String id_kategori;
    SearchView searchView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_produk, container, false);

//        searchView = view.findViewById(R.id.search);
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                loadProduk(s);
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String s) {
//                return false;
//            }
//        });

        spinner = view.findViewById(R.id.kategori);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(spinner.getSelectedItem().toString().equals("Sayur")){
                    loadProduk("1");
                }
                else if(spinner.getSelectedItem().toString().equals("Daging")){
                    loadProduk("2");
                }
                else if(spinner.getSelectedItem().toString().equals("Ikan")){
                    loadProduk("3");
                }
                else if(spinner.getSelectedItem().toString().equals("Rempah")){
                    loadProduk("4");
                }
                else if(spinner.getSelectedItem().toString().equals("Buah")){
                    loadProduk("5");
                }
                else if(spinner.getSelectedItem().toString().equals("Bunga")){
                    loadProduk("6");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadProduk("1");
        return view;
    }

    private void loadProduk(String key){
        API api = ClassURL.getApiClient().create(API.class);
        final Call<List<ProdukModel>> produkCall = api.getProduk(key);
        produkCall.enqueue(new Callback<List<ProdukModel>>() {
            @Override
            public void onResponse(Call<List<ProdukModel>> call, Response<List<ProdukModel>> response) {
                if(response.isSuccessful()){
                    tampilkanProduk(response.body());
                    Log.d("response : ",response.body().toString());
                }else{
                    Log.e("Error Message",response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ProdukModel>> call, Throwable t) {
                Log.e(" Message",t.getMessage());
            }
        });
    }

    private void tampilkanProduk(List<ProdukModel> produk){
        ProdukAdapter produkAdapter = new ProdukAdapter(getContext(), R.layout.produk_item, produk);
        gridView = getActivity().findViewById(R.id.gridView1);
        gridView.setAdapter(produkAdapter);
        produkAdapter.notifyDataSetChanged();
    }

}
