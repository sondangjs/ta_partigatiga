package com.example.sonda.par33.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sonda.par33.Home.DetailProdukActivity;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.Model.pemesananPembeli;
import com.example.sonda.par33.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sonda on 5/6/2020.
 */

public class HomeProdukAdapter extends RecyclerView.Adapter<HomeProdukAdapter.MyViewHolder> {

    ImageView image;
    private List<ProdukModel> pemesananPembelis;
    private Context context;

    public HomeProdukAdapter(List<ProdukModel> pemesananPembelis, Context context) {
        this.pemesananPembelis = pemesananPembelis;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_produk_item, parent, false);
        return new HomeProdukAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
            Picasso.with(context).load(pemesananPembelis.get(position).getGambar()).into(holder.image);
            holder.desc.setText(pemesananPembelis.get(position).getNama_produk());
            holder.harga.setText(pemesananPembelis.get(position).getHarga()+"/"+pemesananPembelis.get(position).getSatuan());

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailProdukActivity.class);
                    intent.putExtra("id_produk", pemesananPembelis.get(position).getId_produk());
                    intent.putExtra("id_pedagang", pemesananPembelis.get(position).getId_pelanggan());
                    intent.putExtra("gambar", pemesananPembelis.get(position).getGambar());
                    intent.putExtra("nama_produk", pemesananPembelis.get(position).getNama_produk());
                    intent.putExtra("harga_produk", pemesananPembelis.get(position).getHarga());
                    intent.putExtra("stok_produk", pemesananPembelis.get(position).getStok_produk());
                    intent.putExtra("id_pedagang", pemesananPembelis.get(position).getId_pelanggan());
                    intent.putExtra("satuan", pemesananPembelis.get(position).getSatuan());
                    intent.putExtra("LayoutCome","1");
                    context.startActivity(intent);
                }
            });
    }

    @Override
    public int getItemCount() {
        return pemesananPembelis.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView desc;
        TextView harga;
        ImageView image;
        CardView cardView;
        public MyViewHolder(View itemView) {
            super(itemView);
            desc = (TextView) itemView.findViewById(R.id.desc);
            harga = (TextView) itemView.findViewById(R.id.harga);
            image = (ImageView) itemView.findViewById(R.id.icon);
            cardView = itemView.findViewById(R.id.cv);
        }
    }
}