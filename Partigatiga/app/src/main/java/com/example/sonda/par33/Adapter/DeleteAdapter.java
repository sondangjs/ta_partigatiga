package com.example.sonda.par33.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.Autentikasi.Register;
import com.example.sonda.par33.CheckRoleActivity;
import com.example.sonda.par33.Home.DetailProdukActivity;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Pedagang.EditProdukActivity;
import com.example.sonda.par33.Pedagang.MenuPedagang;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonda on 4/4/2020.
 */

public class DeleteAdapter extends ArrayAdapter<ProdukModel> {
    Context context;
    public DeleteAdapter(@NonNull Context context, int resource, @NonNull List<ProdukModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.deleteproduk, parent, false
            );
        }

        Picasso.Builder picassos = new Picasso.Builder(context);
        final ProdukModel produk = getItem(position);

        TextView desc = (TextView) convertView.findViewById(R.id.desc);
        TextView harga = (TextView) convertView.findViewById(R.id.harga);
        desc.setText(produk.getNama_produk());
        harga.setText(produk.getHarga()+"/"+produk.getSatuan());
        ImageView image = (ImageView) convertView.findViewById(R.id.icon);
        Picasso picasso = picassos.build();
        picasso.load(produk.getGambar()).into(image);
        Button delete = convertView.findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduk(produk.getId_produk());
            }
        });



        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditProdukActivity.class);
                intent.putExtra("id_produk", produk.getId_produk());
                intent.putExtra("gambar", produk.getGambar());
                intent.putExtra("nama_produk", produk.getNama_produk());
                intent.putExtra("harga_produk", produk.getHarga());
                intent.putExtra("stok_produk", produk.getStok_produk());
                intent.putExtra("satuan", produk.getSatuan());
                intent.putExtra("kategori_produk",produk.getId_kategori());
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    private void deleteProduk(final String id_produk) {
        class DeleteProduk extends AsyncTask<String , Void, String>{
            String res = null;
            @Override
            protected String doInBackground(String... strings) {
                String res = null;
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("id_produk", id_produk));

                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost(ClassURL.URL+"Pedagang/DeleteProduk.php");
                    httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity httEntity = httpResponse.getEntity();
                    if(httEntity != null){
                        InputStream in = httEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        try {
                            while ((line = reader.readLine())!=null){
                                sb.append(line+"\n");
                            }
                            res = sb.toString();
                        }catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                in.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.d("response : ", res);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return res;
            }
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                try{
                    JSONObject obj = new JSONObject(s);

                    if(obj.getBoolean("status")){
                        String response = obj.getString("message");
                        if(response.equals("Successfully Deleted!")){
                            context.startActivity(new Intent(context, MenuPedagang.class));
                        }
                        else{
                            Toast.makeText(context, "Gagal Menghapus Data!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(context, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("error na : "+e.getMessage());
                }
            }
        }
        DeleteProduk delete = new DeleteProduk();
        delete.execute(id_produk);
    }
}