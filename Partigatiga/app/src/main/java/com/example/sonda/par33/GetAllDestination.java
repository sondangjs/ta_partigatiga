package com.example.sonda.par33;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.URLClass.JSONRetrofit;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GetAllDestination extends AppCompatActivity {
    String destinations = "";
    API api;
    int i;
    ArrayList<String> array = new ArrayList<>();
    Button kirim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_all_destination);

        kirim = (Button) findViewById(R.id.kirim);

        final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.search);
        final TextView text = (TextView) findViewById(R.id.textAddress);
        api = JSONRetrofit.getApiClient().create(API.class);
        Call<ResponseBody> result = api.getAllAdress();
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    destinations = new String(response.body().string());
                    String[] datas = destinations.split(";");
                    for(int j=0; j<datas.length; j++){
                        array.add(datas[j]);
//                        System.out.println("Data : "+datas[j]);
                    }

                } catch (IOException e) {
                    System.out.println("Error na : "+e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(GetAllDestination.this, "Terdapat Gangguan! Silahkan Coba Lagi", Toast.LENGTH_SHORT).show();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        autoCompleteTextView.setAdapter(adapter);

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                text.setText(autoCompleteTextView.getText().toString());
                autoCompleteTextView.setText("");
            }
        });

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), GetAddress.class);
                in.putExtra("Tujuan",text.getText().toString().trim());
                startActivity(in);
            }
        });

    }
}
