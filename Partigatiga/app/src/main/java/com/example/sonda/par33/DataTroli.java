package com.example.sonda.par33;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.sonda.par33.Adapter.KeranjangAdapter;
import com.example.sonda.par33.Home.HomeActivity;
import com.example.sonda.par33.Model.KeranjangModel;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Pembeli.LihatTokohPedagang;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DataTroli extends AppCompatActivity {
    GridView grid;
    Button pesan;
    TextView total_keseluruhan;
    ArrayList<String> id_produks = new ArrayList<>();
    ArrayList<String> jumlahs = new ArrayList<>();
    ArrayList<String> nama_produks = new ArrayList<>();
    ArrayList<String> harga_produks = new ArrayList<>();
    ArrayList<String> images  = new ArrayList<>();
    String alamat, id_pedagang, status, id_pembeli, nama_pembeli, kontak_pembeli;
    String id_produk0, id_produk1, jlh_dibeli0, jlh_dibeli1, nama_produk0, nama_produk1, harga0, harga1, gambar0, gambar1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_troli);

        UserModel userModel = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        id_pembeli = userModel.getId();
        nama_pembeli = userModel.getNama();
        kontak_pembeli = userModel.getN0_telp();
        pesan = (Button) findViewById(R.id.pesan);

        total_keseluruhan = findViewById(R.id.total_harga_keseluruhan);
        pesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MakeToast();
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                finish();
            }
        });

        API api = ClassURL.getApiClient().create(API.class);
        final Call<List<KeranjangModel>> keranjang = api.getKeranjang(id_pembeli);
        keranjang.enqueue(new Callback<List<KeranjangModel>>() {
            @Override
            public void onResponse(Call<List<KeranjangModel>> call, Response<List<KeranjangModel>> response) {

                int x=0;
                if(response.isSuccessful()){
                    for(KeranjangModel keranjangModel : response.body()){
                        id_produks.add(keranjangModel.getId_produk());
                        jumlahs.add(keranjangModel.getJumlah());
                        nama_produks.add(keranjangModel.getNama_produk());
                        harga_produks.add(keranjangModel.getHarga_produk());
                        alamat = keranjangModel.getAlamat();
                        id_pedagang = keranjangModel.getId_pedagang();
                        images.add(keranjangModel.getGambar());

                        x+=(Integer.parseInt(keranjangModel.getJumlah()) * Integer.parseInt(keranjangModel.getHarga_produk()));

                    }

                    total_keseluruhan.setText("Total Pembayaran : Rp. "+String.valueOf(x));
                    tampilkanKeranjang(response.body());

                }else {
                    Log.e("Error Message", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<KeranjangModel>> call, Throwable t) {

            }
        });
    }

    private void tampilkanKeranjang(List<KeranjangModel> body) {
        KeranjangAdapter adapter = new KeranjangAdapter(getApplicationContext(), R.layout.keranjang_item, body);
        grid = (GridView) findViewById(R.id.gridView1);
        grid.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void MakeToast(){
        class AddTransaksi extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                id_produk0 = id_produks.toString().replace("[","");
                id_produk1 = id_produk0.replace("]","");
                jlh_dibeli0 = jumlahs.toString().replace("[","");
                jlh_dibeli1 = jlh_dibeli0.replace("]","");
                nama_produk0 = nama_produks.toString().replace("[","");
                nama_produk1 = nama_produk0.replace("]","");
                harga0 = harga_produks.toString().replace("[","");
                harga1 = harga0.replace("]","");
                gambar0 = images.toString().replace("[","");
                gambar1 = gambar0.replace("]","");

                System.out.println(id_produk1);
                System.out.println("JLH : "+jlh_dibeli1);
                System.out.println("Nama : "+nama_produk1);
                System.out.println("Harga : "+harga1);
                status = "DiPesan";
                List<NameValuePair> name = new ArrayList<NameValuePair>();
                name.add(new BasicNameValuePair("id_user", id_pembeli));
                name.add(new BasicNameValuePair("tanggal_beli", getCurrentDate()));
                name.add(new BasicNameValuePair("alamat_beli", alamat));
                name.add(new BasicNameValuePair("id_produk", id_produk1));
                name.add(new BasicNameValuePair("id_pedagang", id_pedagang));
                name.add(new BasicNameValuePair("status_pembelian", status));
                name.add(new BasicNameValuePair("jumlah_dibeli", jlh_dibeli1));
                name.add(new BasicNameValuePair("nama_produk",nama_produk1));
                name.add(new BasicNameValuePair("harga_produk",harga1));
                name.add(new BasicNameValuePair("nama_pembeli", nama_pembeli));
                name.add(new BasicNameValuePair("kontak_pembeli", kontak_pembeli));
                name.add(new BasicNameValuePair("gambar_produk", gambar1));
                try{
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost("http://192.168.43.49/BackendPar33/Pembeli/Transaksi.php");
                    httPost.setEntity(new UrlEncodedFormEntity(name));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity httEntity = httpResponse.getEntity();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return "Data Insert Successfully";
            }
        }
        AddTransaksi add = new AddTransaksi();
        add.execute();
    }

    public String getCurrentDate(){
        DateFormat date = new java.text.SimpleDateFormat("dd-M-yy");
        java.util.Date dates = new java.util.Date();
        String waktu = date.format(dates);
        return waktu;
    }

}
