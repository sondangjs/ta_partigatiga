package com.example.sonda.par33.Pembeli;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sonda.par33.API;
import com.example.sonda.par33.Adapter.ProdukAdapter;
import com.example.sonda.par33.Adapter.ProdukTokoAdapter;
import com.example.sonda.par33.Model.ProdukModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatTokohPedagang extends AppCompatActivity {
    GridView gridView;
    TextView textView;
    TextView telp;
    ImageView called;
    String nama_toko, telepon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_tokoh_pedagang);

        Intent getIntent = getIntent();
        textView = findViewById(R.id.namatoko);
        telp = findViewById(R.id.telp);
        called = findViewById(R.id.call);

        API api = ClassURL.getApiClient().create(API.class);
        final Call<List<ProdukModel>> produkCall = api.getProdukPedagang(getIntent.getExtras().getString("id_pedagang"));
        produkCall.enqueue(new Callback<List<ProdukModel>>() {
            @Override
            public void onResponse(Call<List<ProdukModel>> call, Response<List<ProdukModel>> response) {
                if (response.isSuccessful()) {
                    tampilkanProduk(response.body());
                    for (ProdukModel produk : response.body()) {
                        nama_toko = produk.getNama();
                        telepon = produk.getNo_telepon();
                    }
                    textView.setText(nama_toko);
                    telp.setText(telepon);
                    called.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent call = new Intent(Intent.ACTION_CALL);
                            call.setData(Uri.parse("tel:" + telp.getText().toString()));
                            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(call);
                        }
                    });

                }else{
                    Log.e("Error Message",response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ProdukModel>> call, Throwable t) {

            }
        });
    }

    private void tampilkanProduk(List<ProdukModel> produk){
        ProdukTokoAdapter produkAdapter = new ProdukTokoAdapter(getApplicationContext(), R.layout.produk_item, produk);
        gridView = findViewById(R.id.gridView1);
        gridView.setAdapter(produkAdapter);
        produkAdapter.notifyDataSetChanged();
    }
}
