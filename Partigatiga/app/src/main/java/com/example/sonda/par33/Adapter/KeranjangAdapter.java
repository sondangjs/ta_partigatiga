package com.example.sonda.par33.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sonda.par33.Model.KeranjangModel;
import com.example.sonda.par33.R;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by test on 4/10/2020.
 */

public class KeranjangAdapter extends ArrayAdapter<KeranjangModel> {
    Context context;
    public KeranjangAdapter(Context context, int resource, List<KeranjangModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.keranjang_item, parent, false
            );
        }

        final KeranjangModel keranjangModel = getItem(position);

        Picasso.Builder picassos = new Picasso.Builder(context);
        ImageView produk_image = convertView.findViewById(R.id.produk_image);
        TextView nama_produk = convertView.findViewById(R.id.text);
        TextView harga = convertView.findViewById(R.id.harga);
        TextView jumlah = convertView.findViewById(R.id.jumlah);
        TextView total = convertView.findViewById(R.id.total_harga);

        nama_produk.setText(keranjangModel.getNama_produk());
        harga.setText(keranjangModel.getHarga_produk());
        jumlah.setText(keranjangModel.getJumlah());
        total.setText(String.valueOf(Integer.parseInt(jumlah.getText().toString()) * Integer.parseInt(harga.getText().toString())));
        Picasso picasso = picassos.build();
        picasso.load(keranjangModel.getGambar()).into(produk_image);


        return convertView;
    }

}
