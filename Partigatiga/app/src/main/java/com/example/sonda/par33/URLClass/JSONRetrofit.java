package com.example.sonda.par33.URLClass;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by test on 3/13/2020.
 */

public class JSONRetrofit {
    public static String URL="http://192.168.43.49/AlgoritmaDjikstra/";
    public static Retrofit retrofit;

    public static Retrofit getApiClient() {

        if (retrofit == null) {
            Gson gson = new GsonBuilder().setLenient().create();
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retrofit;
    }
}
