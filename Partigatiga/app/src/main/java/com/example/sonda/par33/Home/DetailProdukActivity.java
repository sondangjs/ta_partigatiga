package com.example.sonda.par33.Home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.Pembeli.Checkout;
import com.example.sonda.par33.Pembeli.Checkout2;
import com.example.sonda.par33.Pembeli.LihatTokohPedagang;
import com.example.sonda.par33.R;
import com.squareup.picasso.Picasso;

public class DetailProdukActivity extends AppCompatActivity {
ImageView gambar;
Button beli;
TextView produkname,hargaproduk,stokproduk, lihatToko;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);

        Picasso.Builder picassos = new Picasso.Builder(getApplicationContext());

        lihatToko = findViewById(R.id.lihatToko);
        gambar = findViewById(R.id.gambar);
        produkname = findViewById(R.id.produkname);
        hargaproduk = findViewById(R.id.hargaproduk);
        stokproduk = findViewById(R.id.stokproduk);
        beli = findViewById(R.id.beli);
//        beli.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        final Intent intent = getIntent();
        produkname.setText(intent.getExtras().getString("nama_produk"));
        hargaproduk.setText(intent.getExtras().getString("harga_produk")+"/"+intent.getExtras().getString("satuan"));
        stokproduk.setText(intent.getExtras().getString("stok_produk"));


        //Lihat Toko
        lihatToko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(),LihatTokohPedagang.class);
                in.putExtra("id_pedagang",intent.getExtras().getString("id_pedagang"));
                startActivity(in);
            }
        });

        Toast.makeText(this, "id_pedagang : "+intent.getExtras().getString("id_pedagang"), Toast.LENGTH_SHORT).show();

        Picasso picasso = picassos.build();
        picasso.load(intent.getExtras().getString("gambar")).into(gambar);
        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(stokproduk.getText().toString())<=0) {
                    Toast.makeText(DetailProdukActivity.this, "Mohon Maaf\nProduk Tidak Tersedia", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(intent.getExtras().getString("LayoutCome").equals("2")){
                        Intent in = new Intent(getApplicationContext(), Checkout.class);
                        in.putExtra("nama_produk", produkname.getText().toString());
                        in.putExtra("harga_produk", intent.getExtras().getString("harga_produk"));
                        in.putExtra("stokProduk", stokproduk.getText().toString());
                        in.putExtra("id_pedagang", intent.getExtras().getString("id_pedagang"));
                        in.putExtra("gambar", intent.getExtras().getString("gambar"));
                        in.putExtra("id_produk", intent.getExtras().getString("id_produk"));
                        startActivity(in);
                    }
                    else{
                        Intent in = new Intent(getApplicationContext(), Checkout2.class);
                        in.putExtra("nama_produk", produkname.getText().toString());
                        in.putExtra("harga_produk", intent.getExtras().getString("harga_produk"));
                        in.putExtra("stokProduk", stokproduk.getText().toString());
                        in.putExtra("id_pedagang", intent.getExtras().getString("id_pedagang"));
                        in.putExtra("gambar", intent.getExtras().getString("gambar"));
                        in.putExtra("id_produk", intent.getExtras().getString("id_produk"));
                        startActivity(in);
                    }
                }
            }
        });
    }
}
