package com.example.sonda.par33.Pedagang;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.API;
import com.example.sonda.par33.Adapter.PemesananPedagangAdapter;
import com.example.sonda.par33.Adapter.PemesananPembeliAdapter;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Model.pemesananPembeli;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DataPemesananPedagangFragment extends Fragment {
    TextView error;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    ProgressBar progressBar;
    private PemesananPedagangAdapter adapter;
    List<pemesananPembeli> transaksiModels;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_data_pemesanan_pedagang, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        fetchPemesanan();



        return view;
    }

    private void fetchPemesanan() {
        API api = ClassURL.getApiClient().create(API.class);
        UserModel userModel = SharedPrefManager.getInstance(getContext()).getUser();
        Call<List<pemesananPembeli>> call = api.getPemesananmPedagang(userModel.getId());
        call.enqueue(new Callback<List<pemesananPembeli>>() {
            @Override
            public void onResponse(Call<List<pemesananPembeli>> call, Response<List<pemesananPembeli>> response) {
                progressBar.setVisibility(View.GONE);
                for (pemesananPembeli pemesan : response.body()){
                    System.out.println("Nama Produk : "+pemesan.getNama_produk());
                }
                transaksiModels = response.body();
                adapter = new PemesananPedagangAdapter(transaksiModels, getContext());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<pemesananPembeli>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Data Tidak Ditemukan", Toast.LENGTH_SHORT).show();
                error.setText("\t\t\tTerjadi Kesalahan! \nSilahkan Periksa Koneksi Anda");
            }
        });
    }


}
