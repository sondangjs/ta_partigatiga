package com.example.sonda.par33;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.Home.HomeActivity;
import com.example.sonda.par33.HomeSeller.HomeSellerActivity;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.Pedagang.MenuPedagang;
import com.example.sonda.par33.Pedagang.TambahProduk;
import com.example.sonda.par33.Pembeli.HomeProduk;
import com.example.sonda.par33.SharedPref.SharedPrefManager;


public class CheckRoleActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private TextView persentase;
    private int Value = 0;
    private static final int STORAGE_PERMISSION_CODE = 4655;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_role);

        storagePermission();

        final UserModel userModel = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        Toast.makeText(this, "Role : "+userModel.getRole(), Toast.LENGTH_SHORT).show();
        progressBar = (ProgressBar) findViewById(R.id.progress);
        persentase = (TextView) findViewById(R.id.persentase);
        progressBar.setProgress(0); //Set Progress Dimulai Dari O

        // Handler untuk Updating data pada latar belakang
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                // Menampung semua data yang ingin diproses oleh thread
                persentase.setText(String.valueOf(Value)+"%");
                if(Value == progressBar.getMax()){
                    if(userModel.getRole().equals("Pembeli")){
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        finish();
                    }
                    else {
                        startActivity(new Intent(getApplicationContext(), MenuPedagang.class));
                        finish();

                    }
                }
                Value++;
            }
        };

        // Thread untuk updating progress pada ProgressBar di Latar Belakang
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    for(int w=0; w<=progressBar.getMax(); w++){
                        progressBar.setProgress(w); // Memasukan Value pada ProgressBar
                        // Mengirim pesan dari handler, untuk diproses didalam thread
                        handler.sendMessage(handler.obtainMessage());
                        Thread.sleep(100); // Waktu Pending 100ms/0.1 detik
                    }
                }catch(InterruptedException ex){
                    ex.printStackTrace();
                }
            }
        });
        thread.start();

    }

    private void storagePermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == STORAGE_PERMISSION_CODE){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
