package com.example.sonda.par33.Autentikasi;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicNameValuePair;
import com.example.sonda.par33.CheckRoleActivity;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class Login extends AppCompatActivity {
    EditText username, password;
    String usernames, passwords;
    ImageView eye;
    Button login, userReg, sellerReg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userReg = (Button) findViewById(R.id.userRegister);
        sellerReg = (Button) findViewById(R.id.SellerRegister);

        userReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = "Pembeli";
                Intent InUser = new Intent(getApplicationContext(), Register.class);
                InUser.putExtra("role", user);
                startActivity(InUser);

            }
        });

        sellerReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String seller = "Pedagang";
                Intent InSeller = new Intent(getApplicationContext(), Register.class);
                InSeller.putExtra("role", seller);
                startActivity(InSeller);
                startActivity(InSeller);
            }
        });

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        eye = (ImageView) findViewById(R.id.eye);
        login = (Button) findViewById(R.id.login);

        //Fungsi Icon Eye
        eye.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch ( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN:
                        password.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case MotionEvent.ACTION_UP:
                        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;
                }
                return true;
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().equals("") || password.getText().toString().equals("")){
                    Toast.makeText(Login.this, "Username dan Password Harus Diisi", Toast.LENGTH_SHORT).show();
                }
                else {
                    usernames = username.getText().toString();
                    passwords = password.getText().toString();
                    loginUser(usernames, passwords);
                }
            }
        });
    }

    private void loginUser(final String usernames, final String passwords) {
        class LoginUser extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                String response =null;

                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("username",usernames));
                nameValuePair.add(new BasicNameValuePair("password", passwords));


                try{
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httPost = new HttpPost(ClassURL.URL+"Autentikasi/login.php");
                    httPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                    HttpResponse httpResponse = httpClient.execute(httPost);
                    HttpEntity entity = httpResponse.getEntity();
                    if (entity!=null){
                        InputStream in = entity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        try {
                            while ((line = reader.readLine())!=null){
                                sb.append(line+"\n");
                            }
                            response = sb.toString();
                        }catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                in.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.d("response : ",response);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try{
                    JSONObject obj = new JSONObject(s);

                    if(obj.getBoolean("status")){
                        String response = obj.getString("message");
                        if(response.equals("Login Gagal! Silahkan Registrasi Diri Anda")){
                            Toast.makeText(Login.this, obj.getString("message"), Toast.LENGTH_SHORT).show();
                            username.setText("");
                            password.setText("");
                        }
                        else{
                            JSONObject userJSON = obj.getJSONObject("DataUser");

                            UserModel user = new UserModel(
                                    userJSON.getString("id_user"),
                                    userJSON.getString("nama"),
                                    userJSON.getString("jenis_kelamin"),
                                    userJSON.getString("alamat"),
                                    userJSON.getString("email"),
                                    userJSON.getString("no_telp"),
                                    userJSON.getString("role"),
                                    userJSON.getString("username"),
                                    userJSON.getString("image")
                            );

                            SharedPrefManager.getInstance(getApplicationContext()).UserLogin(user);
                            finish();
//                            Toast.makeText(Login.this, userJSON.getString("role"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), CheckRoleActivity.class));
                        }
                    }
                    else {
                        Toast.makeText(Login.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        LoginUser login = new LoginUser();
        login.execute(usernames, passwords);
    }
}
