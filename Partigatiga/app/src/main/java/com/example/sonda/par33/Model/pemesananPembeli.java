package com.example.sonda.par33.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sonda on 4/5/2020.
 */

public class pemesananPembeli {
    @SerializedName("id_beli")
    @Expose
    String id_beli;
    @SerializedName("id_user")
    @Expose
    String id_user;
    @SerializedName("tanggal_beli")
    @Expose
    String tanggal_beli;

    @SerializedName("alamat_beli")
    @Expose
    String alamat_beli;
    @SerializedName("total_beli")
    @Expose
    String total_beli;
    @SerializedName("id_produk")
    @Expose
    String id_produk;

    @SerializedName("id_pedagang")
    @Expose
    String id_pedagang;
    @SerializedName("status_pembelian")
    @Expose
    String status_pembelian;
    @SerializedName("nama")
    @Expose
    String nama;

    @SerializedName("no_telepon")
    @Expose
    String no_telepon;

    @SerializedName("nama_produk")
    @Expose
    String nama_produk;
    @SerializedName("harga")
    @Expose
    String harga;
    @SerializedName("gambar")
    @Expose
    String gambar;
    @SerializedName("alamat_pedagang")
    @Expose
    String alamat_pedagang;

    public pemesananPembeli(String id_beli, String id_user, String tanggal_beli, String alamat_beli, String total_beli, String id_produk, String id_pedagang, String status_pembelian, String nama, String no_telepon, String nama_produk, String harga, String gambar, String alamat_pedagang) {
        this.id_beli = id_beli;
        this.id_user = id_user;
        this.tanggal_beli = tanggal_beli;
        this.alamat_beli = alamat_beli;
        this.total_beli = total_beli;
        this.id_produk = id_produk;
        this.id_pedagang = id_pedagang;
        this.status_pembelian = status_pembelian;
        this.nama = nama;
        this.no_telepon = no_telepon;
        this.nama_produk = nama_produk;
        this.harga = harga;
        this.gambar = gambar;
        this.alamat_pedagang = alamat_pedagang;
    }

    public String getId_beli() {
        return id_beli;
    }

    public void setId_beli(String id_beli) {
        this.id_beli = id_beli;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTanggal_beli() {
        return tanggal_beli;
    }

    public void setTanggal_beli(String tanggal_beli) {
        this.tanggal_beli = tanggal_beli;
    }

    public String getAlamat_beli() {
        return alamat_beli;
    }

    public void setAlamat_beli(String alamat_beli) {
        this.alamat_beli = alamat_beli;
    }

    public String getTotal_beli() {
        return total_beli;
    }

    public void setTotal_beli(String total_beli) {
        this.total_beli = total_beli;
    }

    public String getId_produk() {
        return id_produk;
    }

    public void setId_produk(String id_produk) {
        this.id_produk = id_produk;
    }

    public String getId_pedagang() {
        return id_pedagang;
    }

    public void setId_pedagang(String id_pedagang) {
        this.id_pedagang = id_pedagang;
    }

    public String getStatus_pembelian() {
        return status_pembelian;
    }

    public void setStatus_pembelian(String status_pembelian) {
        this.status_pembelian = status_pembelian;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_telepon() {
        return no_telepon;
    }

    public void setNo_telepon(String no_telepon) {
        this.no_telepon = no_telepon;
    }

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getAlamat_pedagang() {
        return alamat_pedagang;
    }

    public void setAlamat_pedagang(String alamat_pedagang) {
        this.alamat_pedagang = alamat_pedagang;
    }
}
