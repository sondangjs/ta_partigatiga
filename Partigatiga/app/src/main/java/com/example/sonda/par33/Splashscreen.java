package com.example.sonda.par33;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.sonda.par33.SharedPref.SharedPrefManager;


public class Splashscreen extends AppCompatActivity {
    SharedPrefManager sharedPrefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sharedPrefManager.CheckLogin();


            }
        }, 5000);
    }
}
