package com.example.sonda.par33.Pedagang;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.Service.ClassURL;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.example.sonda.par33.Splashscreen;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProdukActivity extends AppCompatActivity {
    EditText nama_produk,harga,stok_produk;
    AutoCompleteTextView kategori_produk;
    CircleImageView gambar;
    Button edit;
    String nama_produks, kategori_produks,hargas,stok_produks, id_produks;

    private static final int STORAGE_PERMISSION_CODE = 4655;
    private int PICK_IMAGE_RESULT = 1;
    private Uri filepath;
    private Bitmap bitmap;
    ArrayList<String> kategori = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_produk);

        nama_produk= findViewById(R.id.nama_produk);
        //kategori_produk = findViewById(R.id.kategori_produk);
        harga = findViewById(R.id.harga);
        stok_produk = findViewById(R.id.stok_produk);
        gambar = findViewById(R.id.profile_image);

        gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        Intent intent = getIntent();
        nama_produk.setText(intent.getExtras().getString("nama_produk"));
        harga.setText(intent.getExtras().getString("harga_produk"));
        stok_produk.setText(intent.getExtras().getString("stok_produk"));

        Picasso.with(getApplicationContext()).load(intent.getExtras().getString("gambar")).into(gambar);
        id_produks = intent.getExtras().getString("id_produk");
        //Toast.makeText(this, "id_produk : "+id_produks, Toast.LENGTH_SHORT).show();
        edit= findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage();
            }
        });

//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kategori);
//        kategori.add("Sayur");
//        kategori.add("Daging");
//        kategori.add("Ikan");
//        kategori.add("Rempah");
//        kategori.add("Buah");
//        kategori.add("Burung");
//        kategori_produk.setAdapter(adapter);
//        System.out.println("Kategori Produk : "+kategori_produk.getText().toString());
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_RESULT && data != null && data.getData() != null){
            filepath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                gambar.setImageBitmap(bitmap);
            }
            catch (Exception ex){
                Log.d("Error on : ", ex.getMessage());
            }
        }
    }

    private String getPath(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();

        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Images.Media._ID+"=?", new String[]{document_id},null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }
    private void uploadImage(){

        final UserModel userModel = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        String id_user = userModel.getId();
        String email = userModel.getEmail();

        nama_produks = nama_produk.getText().toString();
//        if(kategori_produk.getText().toString().equals("Sayur")){
//            kategori_produks="1";
//        }else if (kategori_produk.getText().toString().equals("Daging")) {
//            kategori_produks ="2";
//        }else if (kategori_produk.getText().toString().equals("Ikan")) {
//            kategori_produks ="3";
//        }else if (kategori_produk.getText().toString().equals("Rempah")) {
//            kategori_produks = "4";
//        }else if (kategori_produk.getText().toString().equals("Buah")) {
//            kategori_produks = "5";
//        }else if(kategori_produk.getText().toString().equals("Burung")) {
//            kategori_produks = "6";
//        }

        hargas = harga.getText().toString();
        stok_produks = stok_produk.getText().toString();
//        System.out.println("id_kategori : "+kategori_produks);
//        System.out.println("Data ; "+nama_produks);
//        System.out.println("data ; "+hargas);
//        System.out.println("data : "+stok_produks);
//        System.out.println("Data ; "+id_user);

        try {
            String uploadID = UUID.randomUUID().toString();

            new MultipartUploadRequest(this, uploadID, ClassURL.URL+"Pedagang/EditProduk.php")
                    .addFileToUpload(getPath(filepath), "gambar")
                    //.addParameter("id_kategori", kategori_produks)
                    .addParameter("nama_produk", nama_produks)
                    .addParameter("harga", hargas)
                    .addParameter("stok_produk", stok_produks)
                    .addParameter("id_produk", id_produks)

//                    .setNotificationConfig(new UploadNotificationConfig())
//                    .setMaxRetries(3)
                    .startUpload();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Edit Data Produk Berhasil!", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), MenuPedagang.class));
        this.finish();
    }
}
