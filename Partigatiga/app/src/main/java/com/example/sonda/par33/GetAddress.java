package com.example.sonda.par33;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.sonda.par33.URLClass.URLClass;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GetAddress extends AppCompatActivity {
    private TextView tujuan;
    API api;
    String data = "";
    Button jalur;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_address);

        jalur = (Button) findViewById(R.id.getJalur);
        tujuan = (TextView) findViewById(R.id.alamatTujuan);
        Intent maxIntent = getIntent();
        tujuan.setText(maxIntent.getExtras().getString("Tujuan"));


        jalur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String awal = tujuan.getText().toString();
                getAddress(awal , tujuan.getText().toString());
            }
        });

    }

    void getAddress(String awal,String tujuan){
        api = URLClass.getApiClient().create(API.class);
        Call<ResponseBody> responseBodyCall = api.getCoor(awal,tujuan);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    data = new String(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String[] datas = data.split(",");
                Intent in = new Intent(getApplicationContext(), MapsActivity.class);
                in.putExtra("latitude0", datas[0]);
                in.putExtra("longitude0", datas[1]);
                in.putExtra("latitude1", datas[2]);
                in.putExtra("longitude1",datas[3]);
                startActivity(in);
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("'Error Na : "+t.getMessage());
            }
        });
    }
}
