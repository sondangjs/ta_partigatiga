package com.example.sonda.par33.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sonda on 6/14/2020.
 */

public class AlasanModel {
    @SerializedName("id_pemesanan")
    @Expose
    String id_pemesanan;
    @SerializedName("alasan")
    @Expose
    String alasan;

    public AlasanModel(String id_pemesanan, String alasan) {
        this.id_pemesanan = id_pemesanan;
        this.alasan = alasan;
    }

    public String getId_pemesanan() {
        return id_pemesanan;
    }

    public void setId_pemesanan(String id_pemesanan) {
        this.id_pemesanan = id_pemesanan;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }
}
