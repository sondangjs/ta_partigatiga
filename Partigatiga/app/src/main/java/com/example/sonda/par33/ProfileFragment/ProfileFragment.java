package com.example.sonda.par33.ProfileFragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sonda.par33.EditProfile;
import com.example.sonda.par33.GetAllDestination;
import com.example.sonda.par33.Model.UserModel;
import com.example.sonda.par33.R;
import com.example.sonda.par33.SharedPref.SharedPrefManager;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileFragment extends Fragment {
    ImageView setting;
    CircleImageView imageView;
    TextView nama, alamat, phone;
    String namas, alamats, phones, image;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);

        imageView = view.findViewById(R.id.profile_image);
        nama = view.findViewById(R.id.user_name);
        alamat = view.findViewById(R.id.adddres_user);
        phone = view.findViewById(R.id.telephoneUser);
        setting = view.findViewById(R.id.setting);


        UserModel userModel = SharedPrefManager.getInstance(getContext()).getUser();

        nama.setText(userModel.getNama());
        alamat.setText(userModel.getAlamat());
        phone.setText(userModel.getN0_telp());

        namas = nama.getText().toString();
        alamats = alamat.getText().toString();
        phones = phone.getText().toString();
        Picasso.with(getContext()).load(userModel.getImage()).into(imageView);
        image = userModel.getImage();
        System.out.println("Image Link : "+image);

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu dropDown = new PopupMenu(getActivity(), setting);
                dropDown.getMenuInflater().inflate(R.menu.menu_dropdown, dropDown.getMenu());
                dropDown.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.edit:
                                Intent intent = new Intent(getContext(), EditProfile.class);
                                intent.putExtra("nama", namas);
                                intent.putExtra("alamat", alamats);
                                intent.putExtra("phone", phones);
                                intent.putExtra("image", image);
                                startActivity(intent);
                                break;
                            case R.id.logout:
                                new AlertDialog.Builder(getContext())

                                        .setTitle("Konfirmasi")

                                        .setMessage("Anda yakin ingin keluar dari aplikasi?")

                                        .setPositiveButton("YES", new DialogInterface.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                SharedPrefManager sharedPrefManager = new SharedPrefManager(getContext());
                                                sharedPrefManager.logout();
                                                try {
                                                    finalize();
                                                } catch (Throwable throwable) {
                                                    throwable.printStackTrace();
                                                }
                                            }
                                        })
                                        .setNeutralButton("NO", new DialogInterface.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                                break;
                        }
                        return true;
                    }
                });
                dropDown.show();
            }
        });
        return  view;
    }
}
