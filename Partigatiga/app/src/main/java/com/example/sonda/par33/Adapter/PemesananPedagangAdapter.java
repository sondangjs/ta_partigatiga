package com.example.sonda.par33.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sonda.par33.Model.pemesananPembeli;
import com.example.sonda.par33.Pedagang.DetailPemesananPedagang;
import com.example.sonda.par33.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sonda on 4/5/2020.
 */

public class PemesananPedagangAdapter extends RecyclerView.Adapter<PemesananPedagangAdapter.MyViewHolder> {
    ImageView image;
    private List<pemesananPembeli> pemesananPembelis;
    private Context context;

    public PemesananPedagangAdapter(List<pemesananPembeli> pemesananPembelis, Context context) {
        this.pemesananPembelis = pemesananPembelis;
        this.context = context;
    }

    @NonNull
    @Override
    public PemesananPedagangAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pemesanan_item, parent, false);
        return new PemesananPedagangAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PemesananPedagangAdapter.MyViewHolder holder, final int position) {
        holder.cancelBtn.setVisibility(View.GONE);
        if(pemesananPembelis.get(position).getId_produk().contains(",")){
            System.out.println("Data : "+pemesananPembelis.get(position).getTotal_beli()
            );
            StringBuilder builder1 = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            StringBuilder builder3 = new StringBuilder();
            String[] arrayOfNama = pemesananPembelis.get(position).getNama_produk().split(",");
            String[] arrayOfharga = pemesananPembelis.get(position).getHarga().split(",");
            String[] arrayOftotal = pemesananPembelis.get(position).getTotal_beli().split(",");
            String[] arrayOfImage = pemesananPembelis.get(position).getGambar().split(",");

            for(String a : arrayOfNama){
                builder1.append(a);
                builder1.append("\n\n");
            }
            holder.nama_barang.setText(builder1.toString());

            for(String b : arrayOfharga){
                builder2.append(b);
                builder2.append("\n\n");
            }
            holder.harga_barang.setText(builder2.toString());

            for(String c : arrayOftotal){
                builder3.append(c);
                builder3.append("\n\n");
            }
            holder.total_barang.setText(builder3.toString());

            for(int i=0; i<arrayOfImage.length;i++) {
                if(arrayOfImage[i].contains(" ")){
                    View inflate = LayoutInflater.from(context).inflate(R.layout.image_item, null);
                    image = inflate.findViewById(R.id.iv);
                    Picasso.with(context).load(arrayOfImage[i].replace(" ","")).into(image);
                    holder.linearLayout.addView(inflate);
                }
                else {
                    View inflate = LayoutInflater.from(context).inflate(R.layout.image_item, null);
                    image = inflate.findViewById(R.id.iv);
                    Picasso.with(context).load(arrayOfImage[i]).into(image);
                    holder.linearLayout.addView(inflate);
                }
            }

            String hargas = pemesananPembelis.get(position).getHarga().replace(" ","");
            String jumlah = pemesananPembelis.get(position).getTotal_beli().replace(" ", "");
            String[] arrayOfHargas = hargas.split(",");
            String[] arrayJumlah = jumlah.split(",");
            //
            int hartot = 0;
            for (int i = 0; i<arrayOfHargas.length; i++){
                for (int j = i; j<arrayJumlah.length; j++){
                    hartot += (Integer.parseInt(arrayOfHargas[i]) * Integer.parseInt(arrayJumlah[j]));
                }
            }
            holder.harga_total.setText(String.valueOf(hartot));
            holder.status.setText(pemesananPembelis.get(position).getStatus_pembelian());
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailPemesananPedagang.class);
                    intent.putExtra("namapelanggan", pemesananPembelis.get(position).getNama());
                    intent.putExtra("nama_barang",pemesananPembelis.get(position).getNama_produk());
                    intent.putExtra("jumlah_beli", pemesananPembelis.get(position).getStatus_pembelian());
                    intent.putExtra("harga", pemesananPembelis.get(position).getHarga());
                    intent.putExtra("totalBelanja",pemesananPembelis.get(position).getTotal_beli());
                    intent.putExtra("alamat", pemesananPembelis.get(position).getAlamat_beli());
                    intent.putExtra("gambar", pemesananPembelis.get(position).getGambar());
                    intent.putExtra("id_pembelian", pemesananPembelis.get(position).getId_beli());
                    context.startActivity(intent);
                }
            });

        }
        else{
            holder.nama_barang.setText(pemesananPembelis.get(position).getNama_produk());
            holder.harga_barang.setText(pemesananPembelis.get(position).getHarga());
            holder.total_barang.setText(pemesananPembelis.get(position).getTotal_beli());
            View inflate = LayoutInflater.from(context).inflate(R.layout.image_item, null);
            image = inflate.findViewById(R.id.iv);
            Picasso.with(context).load(pemesananPembelis.get(position).getGambar()).into(image);
            holder.linearLayout.addView(inflate);
            holder.harga_total.setText(String.valueOf(Integer.parseInt(pemesananPembelis.get(position).getHarga()) * Integer.parseInt(pemesananPembelis.get(position).getTotal_beli())));
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailPemesananPedagang.class);
                    intent.putExtra("namapelanggan", pemesananPembelis.get(position).getNama());
                    intent.putExtra("nama_barang",pemesananPembelis.get(position).getNama_produk());
                    intent.putExtra("jumlah_beli", pemesananPembelis.get(position).getStatus_pembelian());
                    intent.putExtra("harga", pemesananPembelis.get(position).getHarga());
                    intent.putExtra("totalBelanja",pemesananPembelis.get(position).getTotal_beli());
                    intent.putExtra("alamat", pemesananPembelis.get(position).getAlamat_beli());
                    intent.putExtra("gambar", pemesananPembelis.get(position).getGambar());
                    intent.putExtra("id_pembelian", pemesananPembelis.get(position).getId_beli());
                    context.startActivity(intent);
                }
            });
            holder.status.setText(pemesananPembelis.get(position).getStatus_pembelian());
        }

        holder.txtTolak.setVisibility(View.GONE
        );
    }

    @Override
    public int getItemCount() {
        return pemesananPembelis.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView nama_barang;
        TextView harga_barang;
        TextView total_barang;
        TextView harga_total;
        LinearLayout linearLayout;
        CardView cardView;
        Button cancelBtn;
        TextView status;
        TextView txtTolak;
        public MyViewHolder(View itemView) {
            super(itemView);
            nama_barang = itemView.findViewById(R.id.textView8);
            harga_barang = itemView.findViewById(R.id.textView9);
            total_barang = itemView.findViewById(R.id.textViews);
            harga_total = itemView.findViewById(R.id.editText);
            linearLayout = itemView.findViewById(R.id.imageView2);
            cardView = itemView.findViewById(R.id.cardview);
            status = itemView.findViewById(R.id.textView5);
            cancelBtn = itemView.findViewById(R.id.cancelBtn);
            txtTolak = itemView.findViewById(R.id.textTolak);
        }
    }
}
