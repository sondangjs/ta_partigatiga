package com.example.sonda.par33.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 4/10/2020.
 */

public class KeranjangModel {
    @SerializedName("id_produk")
    @Expose
    String id_produk;
    @SerializedName("id_pedagang")
    @Expose
    String id_pedagang;
    @SerializedName("jumlah")
    @Expose
    String jumlah;
    @SerializedName("nama_produk")
    @Expose
    String nama_produk;
    @SerializedName("harga_produk")
    @Expose
    String harga_produk;
    @SerializedName("gambar")
    @Expose
    String gambar;
    @SerializedName("alamat")
    @Expose
    String alamat;

    public KeranjangModel(String id_produk, String id_pedagang, String jumlah, String nama_produk, String harga_produk, String gambar, String alamat) {
        this.id_produk = id_produk;
        this.id_pedagang = id_pedagang;
        this.jumlah = jumlah;
        this.nama_produk = nama_produk;
        this.harga_produk = harga_produk;
        this.gambar = gambar;
        this.alamat = alamat;
    }

    public String getId_produk() {
        return id_produk;
    }

    public void setId_produk(String id_produk) {
        this.id_produk = id_produk;
    }

    public String getId_pedagang() {
        return id_pedagang;
    }

    public void setId_pedagang(String id_pedagang) {
        this.id_pedagang = id_pedagang;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getHarga_produk() {
        return harga_produk;
    }

    public void setHarga_produk(String harga_produk) {
        this.harga_produk = harga_produk;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
